# Install

The following procedure has been written for Debian 10.

TL;DR:
```sh
git clone --recurse-submodules https://gitlab.com/open-yovo/yovo.git yovo
cd yovo
sh install.sh
```

The full procedure:
```sh
# Clone the YOVO repo
git clone --recurse-submodules https://gitlab.com/open-yovo/yovo.git yovo
cd yovo/

# Install system dependencies
sudo apt update
sudo apt install python3-pip gunicorn3 automake build-essential git libboost-dev libboost-thread-dev libsodium-dev libssl-dev libtool m4 python texinfo yasm i2pd libboost-filesystem-dev libboost-program-options-dev zlib1g-dev

#Install python dependencies
pip3 install -r requirements.txt

# Make MPIR
cd mp_spdz/mpir
./autogen.sh
./configure --enable-cxx
sudo make install

# Expose environment variables to provide MPIR to MP-SPDZ
echo "export LD_LIBRARY_PATH=/usr/local/lib" >> ~/.profile
echo "export LD_RUN_PATH=/usr/local/lib" >> ~/.profile

# Make MP-SPDZ
cd ..
make -j8 spdz2k-party.x
cd ..

# Get I2P
git clone --recursive https://github.com/purplei2p/i2pd-tools i2pd-tools

# Get rights on /etc/i2pd/ for the generation of the config
sudo apt install acl
sudo setfacl -Rm user:$USER:rwx /etc/i2pd/

# Create a leaseFile for the hidden services
cd i2pd-tools
make
./keygen yovo.dat
mv yovo.dat /etc/i2pd/tunnels.conf.d/

# Enable and start I2P
sudo systemctl enable i2pd
sudo systemctl start i2pd
```

# Test
```sh
# For a basic overview
mkdir -p output/i2p_output/
cd src/yovo
pytest

# To test with the coverage report
mkdir -p output/i2p_output/
cd src/yovo
coverage run -m pytest
coverage report --omit=tests/*,*jinja2*,*sphinx-rtd-theme*
coverage html --omit=tests/*,*jinja2*,*sphinx-rtd-theme*
```

# Build the documentation

TL;DR:
```sh
sh build_doc.sh
```

The full procedure:
```sh
python3 scripts/lynter.py
mkdir config
cp src/yovo/config/paths.json config/
cp INSTALL.md docs/general/install.md
cp src/USAGE.md docs/general/usage.md
cp src/BACKEND.md docs/general/backend.md
cp src/FRONTEND.md docs/general/frontend.md
mkdir docs/general/static/
cp -r src/static/images docs/general/static/
make html
```
