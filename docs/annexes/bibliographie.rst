***************************
Bibliographie
***************************

Général
==========

Robert Riemann. Towards Trustworthy Online Voting: Distributed Aggregation of Confidential Data. Cryptography and Security [cs.CR]. Ecole normale supérieure de Lyon, 2017.
https://hal.inria.fr/tel-01675509v3/document

Riemann, Robert, et Stéphane Grumbach. « Distributed Protocols at the Rescue for Trustworthy Online Voting »: Proceedings of the 3rd International Conference on Information Systems Security and Privacy, SCITEPRESS - Science and Technology Publications, 2017, p. 499‑505. DOI.org (Crossref), doi:10.5220/0006228504990505.
https://hal.inria.fr/hal-01501424/document

Chiffrement homomorphe
======================

Mathew Green. « A very casual introduction to Fully Homomorphic Encryption » A Few Thoughts on Cryptographic Engineering, 2 Jan. 2012.
https://blog.cryptographyengineering.com/2012/01/02/very-casual-introduction-to-fully/

Asma MKHININI. « Implantation matérielle de chiffrements homomorphiques » Université Grenoble Alpes, laboratoires TIMA et EμE, 2017.
https://tel.archives-ouvertes.fr/tel-01772355/document

Gentry, Craig. « Computing Arbitrary Functions of Encrypted Data ». Communications of the ACM, vol. 53, no 3, mars 2010, p. 97. DOI.org (Crossref), doi:10.1145/1666420.1666444.
https://crypto.stanford.edu/craig/easy-fhe.pdf

Ivan Damg̊ard, Valerio Pastro, Nigel Smart, and Sarah Zakarias. « Multiparty Computation from Somewhat Homomorphic Encryption ». Advances in cryptology – CRYPTO 2012. 32nd annual cryptology conference, Santa Barbara, CA, USA, August 19–23, 2012.
https://eprint.iacr.org/2011/535.pdf

Secure multy party computation
==============================

David Evans, Vladimir Kolesnikov and Mike Rosulek, A Pragmatic Introduction to Secure Multi-Party Computation. NOW Publishers, 2018.
https://www.cs.virginia.edu/~evans/pragmaticmpc/pragmaticmpc.pdf

Baudron, O., Fouque, P.-A., Pointcheval, D., Stern, J., & Poupard, G. (2001). Practical multi-candidate election system. Proceedings of the Twentieth Annual ACM Symposium on Principles of Distributed Computing - PODC ’01. doi:10.1145/383962.384044

Dimitriou, T., & Michalas, A. (2014). Multi-party trust computation in decentralized environments in the presence of malicious adversaries. Ad Hoc Networks, 15, 53–66. doi:10.1016/j.adhoc.2013.04.013

Sébastien Gambs, Guerraoui Rachid, Harkous Hamza, Florian Huc, Anne-Marie Kermarrec. Scalable and Secure Polling in Dynamic Distributed Networks. 31st International Symposium on Reliable Distributed Systems (SRDS), Oct 2012, Irvine, California, United States. 
https://hal.inria.fr/hal-00723566

Infrastructures
===============

Tor Network https://gitweb.torproject.org/torspec.git/tree/tor-spec.txt Accessed 20 Nov. 2019.
I2P Anonymous Network. https://geti2p.net/en/. Accessed 20 Nov. 2019.
I2P’s Threat Model  https://geti2p.net/en/docs/how/threat-model. Accessed 20 Nov. 2019.
I2P’s vs TOR https://geti2p.net/en/comparison/tor. Accessed 20 Nov. 2019.


Méthode EBIOS (analyse de risques)
==================================

Version 2010 :
https://www.ssi.gouv.fr/uploads/2011/10/EBIOS-1-GuideMethodologique-2010-01-25.pdf
Version 2018, EBIOS risk manager : 
https://www.ssi.gouv.fr/uploads/2018/10/guide-methode-ebios-risk-manager.pdf
https://www.ssi.gouv.fr/uploads/2018/10/fiches-methodes-ebios_projet.pdf
