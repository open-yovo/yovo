**********************
Evènements redoutés
**********************

Les différents événements suivants peuvent impacter le système de vote électronique et compromettre son bon fonctionnement et / ou mettre en défaut les primitives d’anonymat nécessaires. Cette liste a pour but d’être aussi exhaustive sans craindre la redondance.

Dans le tableau suivant, les évènements redoutés sont notés de 1 à 4. 1 représente un niveau faible et 4 fort. Par exemple, un évènement avec un impact de 2 sera moins important qu’un autre avec score égal à 4. Les champs de probabilité comportant un point d’interrogation (« ? ») signifient qu’elle peut grandement varier d’une implémentation technique à l’autre. 

.. Note:: Ajouter le tableau

Avec un certain degré d’abstraction, nous considérons que la falsification du vote et la violation de l’anonymat des utilisateurs sont les deux grandes catégories d’évènements les plus redoutés.
A contrario, le déni de service, ne possédant par ailleurs	 pas de contre-mesure, est relégué à l’arrière-plan. Bien que regrettable, nous avons d’abord souhaité considérer la sécurité des utilisateurs et du vote, plutôt que son bon déroulement.
Dans l’hypothèse où la solution technique retenue requiert la présence de la totalité des participants pour mener à bien le vote, nous admettons la possibilité de réitérer la démarche autant de fois que nécessaire.  

La colonne des probabilités est laissée vide certains évènements redoutés, leur caractérisation ne nous permettant pas d’en évaluer la fréquence. De même, l’absence d’une solution existante empêche une estimation valable de ce chiffre. Pour le reste, nous avons imaginé quels seraient les points d’attaques les plus importants pour l’adversaire quasi-global défini auparavant.

Découverte de la participation
==============================

La caractérisation de l’évènement redouté 3.3, “Découverte de la participation d’un utilisateur au vote”, a été le sujet de nombreuses discussions. En effet, il nous semble important de proposer la fonction démocratique de l’abstention. Se différenciant du vote blanc par le refus du système de vote lui-même, cette possibilité offre néanmoins des métadonnées potentiellement incriminantes. Dans le cadre d’une confrontation en justice, l’impossibilité de prouver la participation à un vote permet ainsi à l’utilisateur de légalement se dédouaner.  
Après de longs débats à ce sujet, nous avons acté qu’il était techniquement impossible de cacher l’abstention d’un utilisateur dans l’ensemble des solutions techniques envisagées. Tout simplement, la présence d’une liste d’utilisateurs (et donc du nombre de votants connu) permet de déterminer la présence d’abstentionnistes ou non, par une simple soustraction des résultats. De même, l’absence d’un utilisateur lors du vote (pour une abstention ou non) pourra potentiellement être détectée si des calculs nécessitent la présence de plusieurs membres. 

Remédiations possibles
======================

La colonne consacrée aux remédiations dans le tableau des évènements redoutés donne un éventail des solutions fonctionnelles ou techniques évoquées. Actuellement, l’utilisation de primitives cryptographiques pour protéger les utilisateurs et le processus de vote lui-même est inévitable. Le choix entre chiffrement asymétrique, homomorphique ou Secure Multi-Party Computing sera déterminé par la suite.

De même, nous considérons la décentralisation comme un point central du projet pour contrer un adversaire quasi-global. La solution précise reste à déterminer dans les prochains documents.

