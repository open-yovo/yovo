﻿Obtention de résultat intermédiaire
====================================

Un acteur curieux ou malintentionné pourrait découvrir les résultats intermédiaires du :term:`vote` et ainsi avoir une idée du résultat final avant les autres utilisateurs. Ce scénario n'impacterait pas directement le résultat du scrutin mais pourrait permettre par exemple à un acteur de décider ou non d'interférer avec le :term:`vote` en fonction de si les résultats penchent dans son intérêt ou non.
