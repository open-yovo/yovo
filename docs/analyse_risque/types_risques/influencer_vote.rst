﻿Influencer l'issue du :term:`vote`
====================================

Coercition
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Une pression extérieure (physique ou morale) peut parfois influer sur le choix d'un utilisateur. L'exemple classique est celui d'une figure d'autorité conseillant, avec une très forte insistance, un employé de voter pour un choix particulier. Dans le cas d'un groupe d'activistes, la violence potentielle du comportement coercitif décuple l'importance de protections contre ce phénomène.
S'il nous est impossible de réduire totalement les possibilités de pressions subies par un utilisateur, nous pouvons néanmoins réduire au maximum la fiabilité de cette démarche. Bien évidemment, il n'est pas ici question de rendre obsolète la torture car le choix conscient d'un utilisateur reste déterminant. La solution devra être un soutien bienveillant pour les votants éclairés et dissuasif pour un organisme malveillant.

Ainsi, la solution doit tendre vers un système rendant techniquement caduque toute tentative de coercition. En conduisant les efforts déployés pour influencer le :term:`vote` d'un utilisateur vers une issue délayée et incertaine, ceux-ci perdent une grande part de leur intérêt.

Bourrage d'urne
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Voter de nombreuses fois sans restriction peut fortement déstabiliser un :term:`vote`, si aucune mesure préventive n'est mise en place. La multiplication de :term:`bulletins` valides permet par exemple de virtuellement gonfler les chiffres d'un choix du :term:`vote`.

Usurpation d'identité
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Dans un système de :term:`vote` où un utilisateur a le choix de déposer un :term:`bulletin` ou non dans l'urne, le cas de l'abstention (à bien différencier du :term:`vote` blanc) doit être pris en compte. Personne ne doit alors être en capacité de voter à la place de l'utilisateur ne souhaitant pas participer.

Lors d'un :term:`vote` réel avec :term:`bulletins` papiers en France, le bourrage d'urne est indissociable de l'usurpation d'identité. Ainsi, on associe un faux :term:`bulletin` à une personne de la liste ne se présentant pas au bureau de :term:`vote`, pour que le subterfuge ne soit pas détecté au dépouillement (correspondance nombre de signatures et nombre de :term:`bulletins` comptés).
