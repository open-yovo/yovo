﻿Déni de service
====================================

Annulation du :term:`vote`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Un ennemi global pourrait mettre en échec le système en forçant l'annulation de chaque :term:`vote` par la falsification d'un des bulletins ce qui forcerait le système à annuler la session de :term:`vote`. En répétant l'opération à chaque session, l'acteur pourrait ainsi paralyser le système électoral de l'organisation cible.

.. Note::
	Ce scénario n'est possible que si le système annule de lui-même le :term:`vote` dès qu'il détecte un bulletin falsifié au lieu de ne pas compter ledit bulletin.

Empêchement du :term:`vote` d'un utilisateur
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Il est possible que l'un des acteurs fasse un déni de service sur l'un des utilisateurs pour l'empêcher de voter sur la durée de l'élection. Un système distribué est par nature plus résilient qu'un système centralisé à ce type d'attaques en répartissant le point de défaillance unique sur l'ensemble des nœuds. 

Une solution possible pour s'assurer qu'une attaque visant à empêcher un ou plusieurs utilisateurs de voter ne puisse pas avoir lieu serait de prolonger le :term:`vote` jusqu'à ce que l'ensemble des utilisateurs aient communiqués leur :term:`vote` ou d'annuler l'élection si tous les utilisateurs n'ont pas communiqué leur :term:`vote`.  Nous choisissons de ne pas retenir cette solution technique car elle permettrait à un individu unique de bloquer l'issu d'un :term:`vote`. Il nous semble également que ce genre de mesures relèverait plus d'un choix organisationnel propre au groupe organisant le :term:`vote` plus qu'un choix technique.
