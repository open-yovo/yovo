﻿Violation d'anonymat
====================================

Découverte de l'identité d'un utilisateur
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Il est nécessaire de protéger l'ensemble des informations pouvant identifier un utilisateur de la solution. Prenons par exemple le cas initial d'un groupe d'activistes prenant une décision. Dans ce cas, la simple participation au :term:`vote` peut entrainer des répercussions graves. La moindre fuite d'information personnelle peut être compromettante et chaque étape du processus de :term:`vote` se doit d'être totalement hermétique.

Le moyen de communication potentiellement utilisé en parallèle du :term:`vote` est considéré comme sécurisé et n'est donc pas pris en compte dans cette analyse de risques.

Découverte du :term:`bulletin` d'un utilisateur
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Un :term:`bulletin` ne doit pas permettre d'identifier son émetteur.
Au-delà du scénario classique de coercition, facilité par l'accès à une preuve de :term:`vote`, un tiers ne doit pouvoir accéder au contenu du :term:`bulletin`. On se base alors dans un scénario où l'infraction est simplement motivée par la curiosité.
