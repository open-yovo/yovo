**********************
Scénarios de risques
**********************

Afin de mettre en contexte les différents types de risques énoncés dans la partie précédente, nous avons réalisés un certain nombre de scénarios qui vont nous aider à définir la probabilité de ces riques et réfléchir à des remédiations possibles.

.. raw:: html
	:file: scenarios.html
