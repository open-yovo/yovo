******************************
Introduction
******************************

Cette analyse de risques vise à cadrer au mieux le projet Système de :term:`vote` électronique anonyme et décentralisé, afin de lui conférer une base stable pour la suite de son déroulé. Nous souhaitons ainsi proposer un socle sur lequel s’appuyer lorsque de futurs questionnements se présenteront et gagner un temps précieux lors de la conception de la solution.
Si ce document s’inspire sur la méthode EBIOS (Expression des Besoins et Identification des Objectifs de Sécurité) modélisée par l’ANSSI, nous avons pris la liberté de l’adapter à nos besoins et contraintes. De fait, nous n’étudions pas un organisme mais une solution technique à mettre en place : le système de :term:`vote`. Le périmètre de l’étude comprend ainsi l’ensemble du processus mais n’est pas étendu à l’organisation même. Ceci réduit grandement la nécessité d’analyse structurelle et nous permet de nous concentrer davantage sur les risques fonctionnels.

.. Warning::
	Ce projet s’inscrit dans le temps. Bien que évitant toute citation trop précise, il est tout à fait envisageable que les primitives cryptographiques mentionnées dans le document ne soient plus d’actualités lors de sa lecture. Nous vous invitons donc à appréhender les propositions techniques sous le prisme de votre réalité cryptographique et non celle de l’année de publication de la présente analyse de risque
