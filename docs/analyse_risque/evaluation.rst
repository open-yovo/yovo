**********************
Evaluation des risques
**********************

Le tableau suivant reprend les évènements redoutés précédent en les réorganisant selon leur impact (gravité) et leur probabilité (vraisemblance). L’organisation graphique permet de mieux appréhender les risques les plus importants.

.. Note:: Ajouter tableau

.. Note::
	Certains risques liés à l’incapacité à procéder au décompte dépendent trop fortement de la solution technique retenue, c’est pourquoi ils sont absents du tableau récapitulatif précédent

.. include:: types_risques/deni_service.rst

.. include:: types_risques/influencer_vote.rst

.. include:: types_risques/resultats_intermediaires.rst

.. include:: types_risques/violation_anonymat.rst