***************
Conclusion
***************

Ce document prend des libertés sur l’application de la méthode EBIOS. Ces divergences s’expliquent du fait que cette méthode est principalement à destination des organisations ou pour un produit fini et est conçu pour être adaptée au besoin de la personne la mettant en œuvre. Notre objectif en utilisant la méthode EBIOS était donc de se donner un exemple de méthodologie d’analyse de risque plutôt que de la suivre à la lettre.  
L’objectif de ce document était dans un premier temps de caractériser et de hiérarchiser les différents risques liés au système de vote puis dans un second temps de commencer à mettre en lumière des remédiations potentielles. Nous pensons avoir atteint ces deux objectifs.  
La première partie a permis de définir au sein du groupe l’importance que nous accordions aux différents critères. Cela a donné lieu à des discussions intéressantes sur la direction que nous souhaitions donner au projet et ce qui nous a permis plus largement de mettre en commun l’objet des recherches que nous avions fait pendant le premier jalon et la période d’entreprise. 
La deuxième partie nous a aidé à percevoir les différentes solutions que nous souhaitons examiner plus en profondeur et à écarter celles qui étaient devenues incompatibles avec nos choix ou qui tout simplement n’étaient plus pertinentes. 
