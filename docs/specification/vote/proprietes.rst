﻿Propriétés d'un :term:`vote` sécurisé
=====================================

Pour qu'un :term:`vote` soit sûr, il définit aussi des propriétés de sécurité :

L'exactitude du :term:`vote`
----------------------------
:Eligibilité: Seuls les votants listés peuvent prendre part au :term:`vote`, et ne peuvent qu'au plus déposer un seul :term:`bulletin`.

:Intégrité: Les :term:`bulletins` ne peuvent être altérés, supprimés ou remplacés.

:Précision: Le résultat final inclue tous les :term:`bulletins` valides et exclue tous les :term:`bulletins` invalides

:Equitabilité: Chaque votant et acteur du :term:`vote` doit posséder exactement les mêmes informations en rapport avec le scrutin


La confidentialité du :term:`vote`
----------------------------------
- Le :term:`vote` doit être secret. On ne doit pas pouvoir lier un votant et son :term:`bulletin`.
- Il ne doit pas y avoir de preuve de :term:`vote` (pas de "*Vous avez voté pour Alice*").

La vérifiabilité du :term:`vote`
--------------------------------
- Soit chaque votant doit pouvoir recompter tous les :term:`bulletins`
- Soit un votant doit pouvoir vérifier que son :term:`bulletin` est bien présent

La robustesse du :term:`vote`
-----------------------------
- L'exactitude et la confidentialité d'un scrutin ne peut pas être compromise par une coalition de taille raisonnable d'agents corrompus ou indisponibles, de votants ou d'équipement de :term:`vote` hors service / corrompus.
- Le :term:`vote` doit être résistant à la coercition
