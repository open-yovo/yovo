Motivations du recours au :term:`vote` électronique 
===================================================

Logistique 
-----------

La mise en place d'un :term:`vote` par :term:`bulletin` papier nécessite de se déplacer
dans un endroit commun prévu à cet effet pour pouvoir voter. Dans le cas
d'organisations importantes, pour un état par exemple, des bureaux de
votes multiples sont généralement créés par zone géographique ce qui
permet aux différents électeurs de voter à proximité de leur domicile.
Dans le cas d'un petit groupe dont les différents membres peuvent être
éclatés géographiquement dans plusieurs pays, l'investissement
temporelle et financier nécessaire pour faire le déplacement jusqu'au
lieu du :term:`vote` peut être dissuasif. 

En plus du simple coût de déplacement,
le fait que les électeurs puissent être de nationalités variées rend
nécessaire le choix d'un pays d'accueil pour l'élection dans lequel la
liberté de circuler librement d'aucun des électeurs ne sera entravée,
que ce soit au titre de sa nationalité d'origine ou de ses actions
passées, comme étape préalable indispensable à tout :term:`vote` physique.

Non aux méta-données : l'anonymat pour les votants
--------------------------------------------------

Il est également à noter que si l'anonymat du contenu du :term:`bulletin` placé
dans l'urne est respectée dans le cadre d'un :term:`vote` physique grâce au
passage à l'isoloir, l'action d'aller voter reste un acte publique : on
s'y présente à visage découvert et l'on procède à la signature d'une
liste électorale, le fait de voyager pour s'y rendre multiplie encore
davantage les traces.

La participation à un :term:`vote` véhicule en soit des méta-données sur le
groupe qui l'organise et sur le sujet du :term:`vote`. Les électeurs peuvent
légitimement pouvoir vouloir éviter d'associer leur identité physique à
leur appartenance au groupe qui organise le :term:`vote`. Cette réserve peut
s'appliquer autant envers des membres extérieurs au groupe qu'envers les
autres électeurs.

De par ces différentes raisons le :term:`vote` physique n'apparait pas comme une
solution satisfaisante dans le contexte d'un petit groupe d'utilisateurs
cherchant à rester discrets et il devient nécessaire de se tourner vers
des solutions dématérialisées et c'est ce qui motive nos recherche envers
un système de :term:`vote` électronique malgré tout les risques inhérents à ce
type de solutions.
