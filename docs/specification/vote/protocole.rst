﻿Définition du protocole de :term:`vote`
=======================================

`Robert Riemann`_ lors de ses travaux a défini les étapes suivantes pour garantir un :term:`vote` sécurisé :

:Enregistrement: Les utilisateurs habilités à voter s'enregistrent auprès de l'autorité de :term:`vote`. Ces utilisateurs enregistrés sont appelés les utilisateurs.
:Vote: Durant la durée de l'élection, les utilisateurs enregistrés peuvent communiquer leurs :term:`bulletins`.
:Regroupement: Les :term:`bulletins` de :term:`vote` contenus dans les différentes urnes sont réunis.
:Décompte: L'ensemble des :term:`bulletins` est comptabilisé et un vainqueur (personne, choix) de l'élection est déterminé en fonction du système de :term:`vote`.
:Vérification: Le protocole de :term:`vote` peut permettre de vérifier le déroulé du :term:`vote` avec le protocole de :term:`vote` existant.

.. _Robert Riemann: https://gitlab.com/open-yovo/ressources/tree/master/GENERAL-Towards_Trustworthy_Online_Voting_Distributed_Aggregation_of_Confidential_Data/Thesis.pdf
