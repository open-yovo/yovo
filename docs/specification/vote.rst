﻿******************************
Le vote
******************************
Les élections par :term:`bulletin` secret existent depuis des millénaires. En Grèce antique déjà lorsque les athéniens procédaient aux votes d'ostracismes, procédure d'exclusion d'un membre de la communauté, ils inscrivaient le nom de la personne à bannir sur une coquille d'huitre ou un cercle d'argile. Cette procédure, plus longue et complexe qu'un simple vote à main levé visait à protéger les électeurs d'éventuels représailles. 

Depuis, le protocole de vote s'est adapté aux évolutions des besoins et des moyens de ses utilisateurs. Ces différentes phases d'expérimentations et de sélections ont permis d'affiner le protocole de vote au fil du temps et ont permis d'ajouter de retirer ou de redécouvrir l'importance de certaines propriétés du protocole de vote. Le remplacement des coquilles d'huitres par le papier une fois que celui-ci est devenu abordable a grandement amélioré les propriétés olfactives du vote. En France il faudra la seconde constitution (constitution de l'an III) pour que la république Française inscrive dans son article 31 que *"Toutes les élections se font au scrutin secret"* . Cette constitution survient après la période de la terreur et les massacres qui y ont eu lieu ont vraisemblablement participés à l'adoption de cet article qui peut nous sembler aujourd'hui évident. 

Le système de vote papier présent aujourd'hui accumule un certain nombre de ces propriétés qui ont été montré nécessaire au cours de l'histoire et un système de vote électronique s'il veut le remplacer devra s'efforcer de présenter des propriétés similaires. 

Nous allons présenter dans un premier temps les propriétés actuelles du vote papier, dans un second temps présenter les problématiques inhérentes au vote papier, nous argumenterons ensuite sur les raisons qui nous poussent malgré tout à explorer ce domaine et nous finirons par présenter les propriétés souhaitables pour notre système de vote électronique.


.. include:: vote/motivations.rst

.. include:: vote/protocole.rst

.. include:: vote/proprietes.rst

Condition d'arrêt
==============================

.. Note::
	Notre solution a pour vocation de s'assurer de la validité des votes dans un sens cryptographique. Les pourcentages présentés (par ex : le vote est annulé pour 5% d'invalidité) ne concernent que les tentatives frauduleuses et non le contenu du vote lui-même.

	Par exemple, si 60% des candidats votent pour la proposition valide A, le vote a bien lieu. En revanche, si 60% des candidats proposent un ballot contrefait, alors le vote est annulé.
