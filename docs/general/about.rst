﻿******************************
A propos
******************************

Ce projet est réalisé dans un cadre universitaire dans un but pédagogique de montée en compétence sur un sujet choisi par le groupe d'étudiant, en l'occurrence la décentralisation d'un réseau et la sécurisation d'un système critique distribué.
