******************************
Glossaire
******************************

.. glossary::
   :sorted:
   
   Bulletin
   Bulletins
      valeur représentant le choix de l'utilisateur parmi les possibilités proposées par le vote.

   Solution
      ensemble des moyens et outils mis en place pour mener à bien le vote électronique.

   Utilisateurs
      ensemble des personnes physiques utilisant la solution.

   Vote
   Scrutin
      session durant laquelle les utilisateurs présents sur une liste prédéfinie communiquent une valeur (voir :term:`bulletin`) correspondant à un concept ou une personne qu'ils soutiennent. Une fois le créneau temporel alloué à la session de vote passé, les :term:`bulletins` sont ouverts informatiquement, les valeurs de vote et le nombre de votants comptabilisés.

   Ballot
      ensemble de :term:`bulletins` de vote.

   Coercion
   	  pouvoir de contraindre quelqu’un à faire quelque chose, ou à le faire d’une certaine façon, ou à ne pas le faire du tout.
