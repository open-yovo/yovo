﻿******************************
Introduction
******************************

Notre sujet porte sur un système de :term:`vote` anonyme, décentralisé et sécurisé. Dans ce cadre, un acteur semi-global peut interférer dans les élections d’un petit groupe d’utilisateurs. Ceux-ci sont à la fois suffisamment érudits pour comprendre et estimer la sécurité de la solution et pour mettre en œuvre toutes les démarches nécessaires à son bon fonctionnement.

Il vient compléter le projet "Solution libre et sécurisée de :term:`vote` en ligne", réalisé par [...]. Le système qu'ils ont mis au point utilisait la solution Belenios, un système de :term:`vote` en ligne anonyme et vérifiable reposant sur la mise en place de tiers de confiance. Dans une optique de décentralisation nous souhaitons éliminer les tiers de confiance l’administration du :term:`vote` et répartir les fonctions qu'ils regroupaient sur les chacun des utilisateurs du système de :term:`vote`.

Pour cela, notre première approche sera de prendre connaissance de l’état de l’art des systèmes existants en regard de l’étude précédemment réalisée. Par la suite, nous souhaiterions axer notre travail sur l’aspect infrastructure plutôt que sur celui cryptographique, comme cela avait été réalisé l’année précédente.
