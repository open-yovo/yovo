candidate
--------------------

.. automodule:: yovo.candidate
	:members:
	:private-members:
	:special-members:
	:show-inheritance:
	:autosummary-no-nesting:
