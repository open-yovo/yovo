******************************
Composantes
******************************

Biens / Supports
================

Canal de communication
----------------------

Afin de permettre le déroulement du vote d’un point de vue organisationnel (communication, planification, annulation, etc.) les utilisateurs utilisent un canal considéré comme sécurisé (cf. hypothèse n°4). Ce canal est décorrélé de la solution technique de vote mais influence toutes les actions prises par les utilisateurs au sujet du scrutin et de son déroulement.

Canal d'anonymisation
---------------------

Les communications entre les postes de vote se feront via un canal de communication spécifique dédié à l’anonymisation des communications afin d’empêcher tout acteur extérieur de remonter aux identités physiques des utilisateurs. Ce canal n’aura pas pour rôle de garantir le chiffrement des informations sensibles transitant sur le réseau, ce rôle sera assumé par notre solution.

Postes de vote
-----------------

Chaque utilisateur aura accès à un poste de vote lui permettant de soumettre son bulletin lors d’un scrutin, procéder au décompte des bulletins et afficher le résultat du scrutin. Ce poste pourra aussi bien être directement la machine physique administrée par l’utilisateur qu’un serveur (ou machine virtuelle) hébergé chez un tier.

Informations
===============

Couples de clés des utilisateurs
--------------------------------

Chaque utilisateur sera doté d’un couple clé publique / clé privé qui lui permettra de s’identifier auprès des autres participants et de procéder aux différentes actions de chiffrement nécessaire à la confidentialité du vote. La partie publique de la clé sera comme son nom l’indique diffusée auprès des autres utilisateurs tandis que la partie privée devra être conservée secrète par l’utilisateur.

Bulletins
---------

Au cours d’un scrutin les utilisateurs vont soumettre leur choix au travers d’un bulletin. Ce bulletin sera conservé et transmis afin de garantir sa confidentialité afin d’éviter toute manœuvre coercitive sur les utilisateurs (cf. propriétés du vote).

Résultat du vote
--------------------

Le résultat du vote constitue une donnée sensible dans la mesure où sa connaissance par un tiers peut entraîner des actions négatives envers l’organisation responsable du scrutin. Par exemple, si le vote concernait la politique appliquée par le groupe d’utilisateurs dans les prochains mois, connaître son résultat pourrait permettre aux adversaires de s’y préparer.

Choix possibles
----------------

De même que pour le résultat du vote, la prise de connaissance des choix proposés lors d’un scrutin pourrait entraîner des actions négatives envers l’organisation responsable du scrutin.
