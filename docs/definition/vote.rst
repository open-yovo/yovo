﻿******************************
Fonctions
******************************

Après une analyse des risques liés au différentes fonctions que nous pouvions implémenter pour ce système, nous avons effectué un certain nombre de choix que nous allons détailler étape par étape. Ici, nous décrivons le principe fonctionnel en précisant son intérêt puis en explicitant la décision finale pour le projet.

.. include:: vote/exactitude.rst

.. include:: vote/confidentialite.rst

.. include:: vote/verifiabilite.rst

.. include:: vote/robustesse.rst

.. include:: vote/simplicite.rst

.. include:: vote/condition_arret.rst

.. include:: vote/vote_blanc.rst
