﻿******************************
Hypothèses du projet
******************************

Cadre et définition du sujet
==============================

Dans le cadre de ce projet, notre approche du :term:`vote` électronique est conditionnée par un ensemble de choix appliqués à un problème général. Par exemple : plutôt que de l'envisager dans un cadre global - étatique -, nous avons choisi de virtualiser le :term:`vote` sous forme d'un collège restreint d'utilisateurs techniquement formés à une technologie potentiellement de pointe.

Les différentes hypothèses et leurs conséquences sont présentées dans les paragraphes suivants. Il est à noter que nous ne garantissons aucunement la sécurité du système présenté hors de ce cadre théorique et que chacune des assertions promulguées est laissée à la discrétion d'un lecteur averti.

Notez par ailleurs que ce projet s'inscrit dans le temps ; il est tout à fait envisageable que les primitives cryptographiques citées dans le document ne soient plus d'actualités lors de sa lecture. Nous vous invitons donc à appréhender les propositions techniques sous le prisme de votre réalité cryptographique et non celle de l'année de publication du présent rapport.

Hypothèses
===========================

1. Le groupe d'utilisateurs est réduit et techniquement apte.
-------------------------------------------------------------

Conséquences:

- La solution n'a pas pour vocation de fonctionner dans un cadre étatique, avec des milliers d'utilisateurs. On ne se soucie donc pas d'une implémentation à grande échelle (accessibilité, résistance à la charge, etc.).
- Les utilisateurs sont tous prêts à investir le temps escompté et ont les compétences techniques nécessaires pour utiliser la solution.
- La solution adoptée ne doit pas se baser sur le nombre pour sa sécurité

2. La liste des utilisateurs est fiable et connue par avance.
--------------------------------------------------------------

Conséquences :

- Les utilisateurs connaissent le nombre théorique des utilisateurs.
- Les utilisateurs ont un moyen de s'identifier entre eux avec certitude

3. Le poste de l'utilisateur est considéré comme fiable.
----------------------------------------------------------

Conséquences:

- Chaque utilisateur de la liste des utilisateurs est en possession d'un système considéré comme sain et sécurisé
- L'utilisateur peut essayer d'utiliser ce système de manière frauduleuse

.. Note::
	Le groupe de projet de l’année précédente utilisait pour cela une clef USB préparée avec le système d’exploitation Tails et l’ensemble des documents nécessaires au :term:`vote`. Nous suivrons certainement cette voie.

4. Les utilisateurs possèdent un canal sécurisé pour communiquer
-----------------------------------------------------------------

Conséquences :

- Les utilisateurs peuvent organiser/discuter/ajourner le :term:`vote` grâce à ce canal

.. Note::

	L'anonymat des utilisateurs ne peut pas être impactée via ce canal. Celui-ci est bien séparé de la solution traitée dans le présent document. Par choix conscient, les problématiques organisationnelles liées au :term:`vote` ne font pas l’objet d’analyses ; celles-ci pourront être étudiées dans de futurs travaux.

.. Tip::
	Exemple de solution : RIOT
