*********************************
Scénarios opérationnels
*********************************

Pour l’ensemble des scénarios nous considérerons que l’utilisateur principal est Bob et notre solution sera désignée par « le système ».

Démarrage du scrutin
====================

- Chaque poste utilisateur possède la liste des votants (la liste des identifiants sur le réseau des votants) ainsi que la liste des clés publiques leur permettant de s’authentifier sur le système en signant leurs paquets.
- Lors de sa connexion au réseau I2P chaque poste tente d’établir une connexion avec les autres postes.
- Lorsque tous les postes répondent correctement, le système signale que le scrutin est prêt à démarrer.
- Le système génère un couple de clés publique / privée qui serviront pour le chiffrement homomorphique. La clé publique est connue de tous en revanche la clé privée est partagée par tous les postes sans qu’aucun ne la connaisse en entier.

Scrutin
=========

- Bob démarre son poste lui permettant de voter.
- Bob se connecte au réseau des votants avec le protocole I2P.
- Bob choisit son bulletin.
- Le système découpe son bulletin et distribue les différentes parties chiffrées avec la clé publique pour le chiffrement homomorphique à tous les autres votants.
- Le système récupère le signal de retour des autres postes confirmant que le bulletin à bien été distribué à chacun des utilisateurs.
- Le système confirme à Bob que son bulletin à bien été distribué.
- Le système signale que son vote a été envoyé et qu’il est prêt à décompter à tous les autres postes et les autres postes marquent le poste de Bob comme ayant déjà voté.
- Une fois tous les postes prêts à décompter ou une fois l’heure fixée pour la fin du scrutin atteinte, le système bloque tout nouveau scrutin.
- Les postes procèdent à l’agrégation des votes chiffrés grâce aux propriétés homomorphiques du chiffrement utilisé.
- Les postes procèdent ensemble au déchiffrement du résultat en partageant la clé de déchiffrement dont chacun possède une partie.
- Chaque poste vérifie individuellement les calculs et partage aux autres postes son résultat.
- Tous les postes vérifient que les calculs n’ont pas été manipulés.
- Le résultat du scrutin est affiché à Bob accompagné des preuves de calcul.

.. image:: /static/scenario_2.png

Levée d’erreur
===============

- Un des postes détecte une erreur dans le scrutin.
- L’erreur est affichée à l’utilisateur.
- L’erreur est broadcastée à tous les autres postes.
- Le scrutin est stoppé.

Bourrage d’urne / Usurpation d’identité
=======================================

- Bob change d’avis et choisit de resoumettre son vote.
- Bob choisit un nouveau bulletin.
- Le système découpe son bulletin et distribue les différentes parties à tous les autres votants de manière sécurisée.
- Les autres postes reçoivent le bulletin et constatent que le poste source du bulletin a déjà voté.
- Le système récupère les messages de refus des autres postes.
- Le système lève une erreur.
Cette approche permet aussi d’informer un utilisateur si un autre à voter à sa place. Il pourra alors réagir en prévenant les membres de l’organisation et annuler le scrutin.

Coercion
=========

- Alice fait pression sur Bob pour qu’il vote selon ses instructions.
- Bob ne veut pas voter selon les instructions d’Alice mais décide de participer au vote.
- Bob effectue son vote en son âme et conscience.
- Bob récupère le message de confirmation que son vote a bien été distribué.
- Bob récupère le résultat du scrutin et les preuves de calcul.
- Bob transmet les preuves de calcul et le message de confirmation à Alice.
- Alice est incapable de déduire le Bulletin de Bob à partir des informations transmises.

Vérifier la prise en compte du bulletin
=======================================

- Une fois le bulletin découpé, le système affiche le hash des bulletins à Bob.
- Le système envoie les morceaux aux autres postes.
- Les autres postes affichent à tous les votants le hash des morceaux reçus.
- Bob peut vérifier que chaque morceau de son vote a bien distribué à tous les autres postes.

Arrêt d’un poste d’utilisateur
==============================

- Au cours du scrutin un des postes se déconnecte.
- Un des autres postes tente de se connecter à celui-ci.
- Le poste lève une erreur.
