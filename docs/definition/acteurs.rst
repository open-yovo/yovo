******************************
Acteurs
******************************

Nous avons défini trois groupes d’acteurs principaux. Nous nous référerons à ces termes dans la suite du document.

Utilisateurs
==============

Un groupe d’individus devant prendre une décision collégiale en sélectionnant un candidat parmi un ensemble d’idées ou de personnes. Dans ce cadre, les utilisateurs ne se sont potentiellement pas rencontrés physiquement (et ne le peuvent pas) et leur participation au :term:`vote` même est une information sensible.

Adversaires quasi-globaux
===========================

Munis de moyens techniques, humains et financiers potentiellement infinis, ce groupe d'individus souhaiter perturber le :term:`vote`. La présente analyse de risque vise à déterminer dans quelle mesure (annulation, falsification, etc.) ils pourront s’y adonner.

.. Note::
	Nous les qualifions de "quasi-globaux" car, dans le cadre d'un réseau décentralisé, ce groupe ne pourra contrôler l'ensemble des noeuds mais seulement un certain pourcentage. Dans le meilleur des cas, la solution a pour vocation de supporter un contrôle à N-1 noeuds (avec N le nombre total de noeuds) mais cette hypothèse semble très difficile à mettre en place techniquement et nous nous concentrerons certainement sur un pourcentage de contrôle diminué.

Environnement
=================

Cet acteur peut avoir de graves répercussions sur le déroulé du :term:`vote`. Il symbolise l’ensemble des manifestations naturelles ou techniques pouvant interférer avec le processus ne provenant pas des adversaires quasi-globaux. Malgré un aspect davantage symbolique, l’environnement interagit bien avec la solution et ses utilisateurs c’est pourquoi nous le considérons comme un acteur.
