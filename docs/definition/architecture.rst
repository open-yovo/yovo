*************************
Architecture
*************************

Calcul multipartie sécurisé
============================

Dans un monde parfait, pour effectuer un calcul sans que les participants n’aient à révéler leur entrée aux autres, il suffit d’utiliser un tiers de confiance : celui-ci récolte les différentes données, effectue le calcul souhaité et ne renvoie que le résultat.
Le principe même du calcul multipartite sécurisé, ou Secure Multi-party Computation (SMC), est de permettre le calcul d’une fonction commune sans que les participants ne connaissent les entrées des autres et sans usage d’un tiers de confiance. 

Notre choix s’est porté sur le SMC pour plusieurs raisons :

- Il permet de se passer d’un tiers de confiance ;
- Il permet de garder les entrées des utilisateurs secrètes ;
- Certains protocoles de SMC permettent une résilience jusqu’à N-1. Ainsi, le calcul reste valide tant qu’un seul utilisateur est bienveillant. Les solutions à N-1 utilisent en général une combinaison de SMC et de cryptographie homomorphe.

Pour la mise en place du SMC, tous les membres doivent pouvoir communiquer entre eux. Cela limite donc le nombre de participant, le SMC demandant de nombreuses communications.

Infrastructure réseau
=====================

Il est nécessaire dans le cadre de notre projet d’anonymiser les communications. Pour cela nous avons comparé plusieurs solutions techniques (voir tableau comparatif en annexes). 
La solution retenue est I2P (The Invisible Internet Project), une solution éprouvée et bien documentée (documentation technique complète, ainsi qu’une analyse de risque). I2P permet de rejoindre un réseau pair-à-pair anonymisant, sous forme de réseau superposé : il est construit au-dessus d’Internet. 
Au contraire de Tor, I2P n’est pas ouvert vers internet, et permet un hébergement de ressource un peu plus simple que Tor. Ce système a par contre l’inconvénient de ne pas être accessible instantanément ; il faut en effet un certain temps pour que le routage soit effectif. 

Le projet permet à deux machines connectées au réseau I2P de communiquer sans connaître leurs adresses IP, ce qui est un réel avantage pour les utilisateurs. De plus dans le cadre du vote, comme chaque nœud du réseau I2P est un relai potentiel, I2P permet d’introduire du bruit au sein de nos communications rendant une écoute plus difficile. 

Le fonctionnement d’I2P présente de nombreux avantages :

- Chaque message transitant en son sein est chiffré ;
- Les utilisateurs sont identifiés par une clé cryptographique ;
- Routage en ail (garlic routing) très similaire au routage en oignon de Tor ; ceci rendant difficile de connaître l’origine d’un message ;
- Chaque participant à I2P relaie lui aussi des messages (au contraire de Tor qui fonctionne par nœuds d’entrée / sortie, par exemple) ;
- La table de routage (NetdB) est distribuée au sein du réseau en utilisant un système de table de hash distribuée permettant donc une résilience élevée et sans autorité centrale.

Tout comme dans Tor, les messages dans I2P effectuent plusieurs sauts avant d’arriver à destination ; dans I2P, le message retour empruntera une autre route et effectuera aussi plusieurs sauts. A noter qu’I2P ralentira les communications entre les différents utilisateurs du système de vote, chaque saut ralentissant la communication. 

L’objectif est donc d’utiliser I2P comme support pour le calcul multipartite sécurisé :

.. image:: /static/archi_basic.png
