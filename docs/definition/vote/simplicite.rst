Simplicité du protocole de vote
===============================

Définition
----------

La simplicité offre un degré de transparence à l'utilisateur final en lui permettant d'émettre un jugement autonome sur le système de vote. Elle permet également au plus grand nombre d'évaluer la sécurité du système en un minimum de temps, ce qui facilite la mise en évidence de failles de sécurité et améliore le niveau de sécurité global du système.

Intérêt
-------

Comme mentionné plus haut, la caractéristique essentielle du système de vote est sa capacité à fédérer la confiance de tous ses utilisateurs. Cette confiance s'obtient de différentes manières en fonction du type d'utilisateurs. L'utilisateur techniquement inapte placera la transparence du protocole de vote comme facteur principal car elle lui permet de garder le contrôle. Pour l'utilisateur plus technique, ce sera plutôt la robustesse théorique et pratique qui primera. Nous allons désormais montrer que la simplicité est une propriété souhaitable pour ces deux objectifs.

La simplicité comme un critère de transparence
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

La littérature portant sur le sujet du vote électronique apporte une importance toute particulière à la simplicité du système de vote lorsque celui-ci concerne une population hétérogène comme, par exemple, un état. Voici un raisonnement pouvant selon nous justifier la prédominance de ce critère.

Une solution technologiquement avancée permettant d'éliminer un tiers de confiance revient à placer une confiance aveugle dans la technologie utilisée pour la majorité des utilisateurs. En effet, un système de vote réparti idéal mais technologiquement complexe sera opaque pour l’utilisateur inapte à comprendre son fonctionnement. Incapable d'évaluer de lui-même la sécurité du système de vote, l'utilisateur devra se référer au jugement d'experts pour accorder ou non sa confiance au système de vote. L'utilisateur privé de son autonomie à émettre un jugement pertinent sur le système de vote est de nouveau contraint de faire confiance à un tiers - ici, l'expert technique. 

Le problème de l'expert technique est accentué en cas de conflits d'intérêts autour du système, par exemple, quand les personnes faisant la communication autour de la solution de sécurité en sont ses créateurs. On peut imaginer que ce levier soit encore accentué dans le cas d'une solution commerciale.

La simplicité comme un critère de sécurité
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sur le plan de la sécurité, il est aussi souhaitable de se baser sur un protocole simple pour qu'il soit facilement auditable par le plus grand nombre afin de maximiser les chances de découvrir rapidement une faille de sécurité. Pour illustrer ce principe, on peut citer l'exemple du développement logiciel dans lequel il est considéré qu'une application dont le code est open source sera plus sécurisable qu'une dont le code serait propriétaire et obfusqué. 

Un protocole faisant appel à des primitives complexes limitera ainsi la quantité de ses auditeurs potentiels en retirant aux personnes n'ayant pas la compétence requise la possibilité d'éprouver efficacement le protocole. De même, le protocole retenu devrait s'efforcer d'être le plus claire possible afin de maximiser l'efficacité des personnes qui prendront le temps de l’auditer.

Choix
-----

Malgré les constats précédents, il est aujourd’hui impossible de trouver un équivalent simple au bulletin papier déposé dans une urne sans passer par des tiers de confiance. Toute solution technique décentralisée met en jeu des primitives cryptographiques parfois complexes et, d’après la littérature du domaine, une application accessible est à l’heure actuelle inconcevable. L'hypothèse de départ selon laquelle les utilisateurs sont techniquement aptes nous permet de contourner cette problématique dans le cadre du projet de cette année. Néanmoins, elle reste bloquante pour la démocratisation de tout système de vote en ligne et elle pourra faire l’objet de recherches ultérieures. De notre côté et afin de tout de même traiter au mieux cet aspect, nous mettons l’accent sur la transparence au travers de l’ouverture du code source pour permettre son analyse. La complexité des protocoles envisagés pourra également servir de critère pour départager des solutions équivalentes sur les autres propriétés.