Vérifiabilité du vote
=========================

Définition
----------

Le résultat du scrutin doit être vérifiable par l’utilisateur. Pour cela, deux modes de fonctionnement sont envisageables : 

- Chaque utilisateur a la capacité de recompter tous les bulletins ;
- Chaque utilisateur est en mesure de vérifier que son bulletin est bien pris en compte, dans le cas contraire il peut s’opposer au résultat de l’élection. Pour cela il doit pouvoir obtenir une preuve que son vote n’a pas été pris en compte. Pour être compatible avec la propriété de confidentialité, cette preuve ne doit pas révéler le bulletin de l’utilisateur.

Le premier cas est idéal car il offre une visibilité maximale à l’utilisateur en assurant que le dépouillement global s'est correctement effectué. Le second cas ne propose qu’un moyen de vérification individuel et non sur le résultat lui-même. Il se base donc sur le fait que les autres utilisateurs réagiront si leur ballot a été truqué.

Intérêt
-------

Par cette propriété, les utilisateurs sont certains de pouvoir vérifier de manière empirique le résultat du vote. Ainsi, ils peuvent avoir confiance en la légitimité de la décision prise en s’appuyant sur le résultat du vote pour représenter l’avis de la majorité des utilisateurs.
En effet, sans ces propriétés, un utilisateur n’a pas de garantie que son bulletin aura correctement été pris en compte et aura participé au même titre que celui de tous les autres électeurs au résultat du vote. Ces propriétés représentent donc un des fondements d’un vote démocratique.

Choix
-----

Chaque utilisateur pourra par lui-même recompter tous les bulletins, ce qui lui permettra de s’assurer que le décompte n’a pas été perturbé par une tentative de fraude. Cela est garanti par le fait que tous les bulletins sont distribués à tous les utilisateurs et que le décompte est effectué par tous les utilisateurs au même moment.