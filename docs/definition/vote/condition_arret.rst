Condition d'arrêt
=========================

Définition
----------

Le protocole doit définir les conditions lors desquelles il ne peut plus fonctionner correctement et annuler l’ensemble des opérations faites précédemment afin d’empêcher toute fraude et toute manipulation des résultats.

Intérêt
-------

Dans une optique d’accord de confiance entre les utilisateurs et le protocole de vote, l’ensemble des règles régissant son déroulement mais aussi ses limites est un prérequis nécessaire. 

Choix
-----

Nous avons défini dans l’analyse de risques (voir en annexes) différentes conditions d’arrêt technique. L’incapacité d’un utilisateur à participer au dépouillement ou la détection d’un bulletin frauduleux sont deux exemples. Ces principes supposent un rejeu du vote jusqu’à sa bonne réalisation plutôt que l’acceptation d’un verdict erroné. 