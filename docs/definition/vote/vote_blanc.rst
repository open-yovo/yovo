Vote blanc / Abstention
=========================

Définition
----------

Le vote blanc correspond à la sélection d’aucun des candidats présents sur la liste proposée lors du scrutin. L’utilisateur participe donc au vote, mais considère qu’aucun des choix présents ne lui convient. 
A contrario, l’abstention a lieu lorsque l’utilisateur rejette le système de vote lui-même, et non les candidats proposés. Pour communiquer son désaccord, il n’est alors plus question de participer au vote d’une quelconque manière.

Intérêt
-------

Il est nécessaire de correctement distinguer ces deux principes, qui peuvent être supportés ou non indépendamment, selon le système de vote retenu. Ainsi, ces fonctions ne sont pas toujours prises en compte, comme dans le scrutin national français sur papier. 

Choix
-----

D’un point de vue démocratique, il nous semble important qu’un utilisateur puisse communiquer son désaccord, autant avec la liste de candidats qu’au sujet du système lui-même. 
Le cas du vote blanc est relativement simple à implémenter techniquement à partir du moment où la liste de candidats peut en compter plus de deux. Il suffit alors d’ajouter une option “vote blanc” à côté des choix initiaux. L’abstention se basant sur la liste des utilisateurs, on peut aisément déterminer le nombre d’abstentionnistes en soustrayant le nombre de bulletins finaux à celui des inscrits. 
