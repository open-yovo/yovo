Exactitude du vote
=========================

Définition
----------

Pour qu’un vote soit considéré comme exact, il doit répondre à un ensemble de propriétés de sécurité :

- Eligibilité : seuls les utilisateurs listés peuvent prendre part au vote, ils peuvent au plus déposer un seul bulletin.
- Intégrité : les bulletins ne peuvent être altérés, supprimés ou remplacés.
- Précision : le résultat final inclue tous les bulletins valides et exclue tous les bulletins invalides.
- Equitabilité : chaque votant et acteur du vote doit posséder exactement les mêmes informations en rapport avec le scrutin.

Intérêt
-------

Cette liste de points doit donner à l’utilisateur un sentiment d'équité dans le vote afin de légitimiser le candidat retenu. Elle constitue le fondement de l’outil démocratique qu’est le système de vote et est similaire à ce qui peut se retrouver dans un vote par bulletin papier ou même à main levé.

Choix
-----

Toute solution envisagée doit pouvoir respecter et garantir cette propriété fondamentale. Une solution ne les respectant pas ne pourrait pas remplir correctement sa fonction de vote et serait donc disqualifiée.