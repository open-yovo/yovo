Confidentialité du vote
=======================

Définition
----------

Le vote et son contenu doit être secret afin de permettre un choix libre de l’utilisateur. On ne doit donc pas pouvoir lier un utilisateur et son bulletin et encore moins connaitre le contenu de ce bulletin. Il ne doit pas non plus y avoir de preuve de vote. C'est-à-dire qu'aucun des participants ne doit obtenir une preuve tangible qu’il a voté pour tel ou tel candidat.
Idéalement le système ne devrait pas non plus permettre de savoir qui a participé au vote.

Intérêt
-------

Bulletin secret
^^^^^^^^^^^^^^^^

Vote et bulletin secret ne vont pas forcément de pair. Pour des raisons de responsabilisation des électeurs ou même de praticité certains modèles de vote mettent en place un scrutin public. Le vote à main levé peut servir pour illustrer ces deux nécessités : l’Assemblée Nationale française fait la majorité de ses votes à mains levées ou en tout cas de manière publique par souci de transparence des élus vis-à-vis des électeurs qu’ils représentent. Pour les assemblées directes, les Landsgemeinde , assemblées de citoyens d'une commune ou d’un canton suisse, se font sur la place publique et le vote à main levée permet d’utiliser un minimum de matériel. De même, les votes de certaines associations, notamment étudiantes, se font à main levée ce qui permet de limiter le matériel nécessaire à l’élection.

À l'exception de ces cas particuliers, le vote secret reste tout de même largement plébiscité car il permet à l'électeur d'exprimer son opinion en limitant toute forme d'influence extérieure. Ces influences sont nombreuses : conformisme, intimidation, subordination, coercition ou crainte de représailles. Conserver le bulletin de vote secret est un enjeu de longue date ; la république Athénienne faisait déjà ses votes de d'ostracisme, visant à bannir un individu – donc particulièrement propices aux représailles - de manière secrète. En France, l'article 31 de la constitution de l'an III (qui a été voté à la sortie de la Terreur) prévoit que "toutes les élections se [fassent] au bulletin secret". 		
Dans le cas d'un vote physique, l'isoloir permet de garantir l'anonymat du bulletin car il est aisé de vérifier que l'électeur est rentré seul dans l'isoloir. Dans le cas d'un vote électronique se faisant "à domicile", rien ne permet de garantir que l'électeur soit seul devant son terminal ; il devient donc nécessaire de mettre en place des moyens particulier pour lutter contre la coercition.

Preuve de vote
^^^^^^^^^^^^^^

L’absence de preuve de vote a le double objectif de limiter les risques liés à la coercition d’un utilisateur et ceux liés à la vente de bulletin. Dans ces deux cas, l'absence de preuve de vote force soit le contraigneur soit le commanditaire à croire sur parole l’utilisateur lui affirmant avoir voté pour un certain candidat. In fine, exercer une pression physique ou morale sur un participant perd donc de son intérêt.

Participation au vote
^^^^^^^^^^^^^^^^^^^^^^^

Si l’anonymat du contenu du bulletin placé dans l’urne est respecté dans le cadre d’un vote physique grâce au passage à l’isoloir, l’action d’aller voter reste un acte public : on s’y présente à visage découvert et l’on procède à la signature d’une liste électorale.
La participation à un vote véhicule en soi des métadonnées sur le groupe qui l’organise et sur le sujet du vote. Les utilisateurs peuvent légitimement vouloir éviter d’associer leur identité physique à leur appartenance au groupe qui organise le vote. Cette réserve peut s’appliquer autant envers des membres extérieurs au groupe qu’envers les autres électeurs.
Le système retenu doit donc préserver l’anonymat des utilisateurs et éviter de fournir une preuve tangible de leur participation au vote. Cela leur permettrait ainsi de faire valoir leur présomption d’innocence dans le cadre d’un procès qui chercherait à montrer leur participation à un vote ayant servi à la prise de décision d’une action légalement répréhensible.

Choix
-----

La solution retenue devra être à même de garantir la confidentialité des bulletins afin de permettre la libre expression de l’opinion des utilisateurs. Les contraintes de praticité et de représentativité pouvant mener à l’abandon du bulletin secret ne sont pas pertinentes dans notre cas, le système de vote mis au point ne visant pas à être pratique d’utilisation et les utilisateurs exprimant à priori leur opinion propre sans avoir à en rendre compte auprès d’un autre groupe d’individus. 
La solution retenue devra également tenter de mettre en place des mesures limitant les possibilités de coercition ou, a minima, empêcher l’émission de preuve de vote ou de traces pouvant servir de preuve de vote.
Vis-à-vis de la participation au vote, le système retenu protège l’identité numérique des utilisateurs en masquant leur adresse IP mais il ne masque pas la participation des utilisateurs au vote. Un système qui parviendrait à remplir cette fonction serait donc une amélioration.
