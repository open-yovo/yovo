Robustesse du protocole de vote
===============================

Définition
----------

L’exactitude et la confidentialité d’un scrutin ne peut pas être compromise par une coalition de taille raisonnable d’agents corrompus ou indisponibles, d’utilisateurs ou d’équipements de vote hors service / corrompus.

Intérêt
-------

Dans un contexte hostile voire très hostile, il est important que le système soit résilient jusqu’à un certain point à l’adversité. Ce taux doit être défini à l’avance et le système doit garantir qu’en dessous de ce taux le résultat du scrutin ne pourra être altéré sans alerter les utilisateurs. Grâce à cette propriété, les utilisateurs du système peuvent l’utiliser en toute connaissance de cause et prendre des mesures pour ne pas dépasser ce taux de menace acceptable.

Choix
-----

Le système retenu permet à un unique utilisateur de vérifier si le vote s’est correctement déroulé même si tous les autres utilisateurs sont malveillants et coopèrent entre eux. Pour N utilisateurs, le système supporte donc jusqu’à N-1 utilisateurs malveillants.