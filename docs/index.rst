.. Warning::
   Ce document n'est pas terminé et est complété au fûr et à mesure par les participants au projet.

.. include:: ../README.rst

.. toctree::
   :caption: Général
   :maxdepth: 2

   general/presentation
   general/install
   general/usage
   general/backend
   general/frontend
   general/glossaire
   general/about

.. toctree::
   :caption: Définition du projet
   :maxdepth: 2

   definition/hypotheses
   definition/vote
   definition/acteurs
   definition/composantes
   definition/scenarios
   definition/architecture

.. toctree::
   :caption: Spécifications du projet
   :maxdepth: 2

   specification/vote

.. toctree::
   :caption: Analyse de risque
   :maxdepth: 2

   analyse_risque/analyse_risque
   analyse_risque/evenements
   analyse_risque/scenarios
   analyse_risque/evaluation
   analyse_risque/conclusion

.. toctree::
   :caption: AutoDoc
   :maxdepth: 2

   autodoc/vote
   autodoc/candidate
   autodoc/host
   autodoc/main
   autodoc/interface
   autodoc/utils

.. toctree::
   :caption: Annexes
   :maxdepth: 2

   annexes/bibliographie
   annexes/pylint
