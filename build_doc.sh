#!/bin/sh

python3 scripts/lynter.py
mkdir config
cp src/yovo/config/paths.json config/
cp INSTALL.md docs/general/install.md
cp src/USAGE.md docs/general/usage.md
cp src/BACKEND.md docs/general/backend.md
cp src/FRONTEND.md docs/general/frontend.md
mkdir docs/general/static/
cp -r src/static/images docs/general/static/
make html
