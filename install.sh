#!/bin/sh

#Install dependencies 
sudo apt update
sudo apt install python3-pip gunicorn3 automake build-essential git libboost-dev libboost-thread-dev libsodium-dev libssl-dev libtool m4 python texinfo yasm i2pd libboost-filesystem-dev libboost-program-options-dev zlib1g-dev

#Install python dependencies
pip3 install -r requirements.txt

#Make MPIR
cd mp_spdz/mpir
./autogen.sh
./configure --enable-cxx
sudo make install

# expose environment variable to rpovide MPIR to MP-SPDZ
echo "export LD_LIBRARY_PATH=/usr/local/lib" >> ~/.profile
echo "export LD_RUN_PATH=/usr/local/lib" >> ~/.profile

#Make MP-SPDZ
cd ..
make -j8 spdz2k-party.x
cd ..

#Prepare I2P
git clone --recursive https://github.com/purplei2p/i2pd-tools i2pd-tools

#Get rights on /etc/i2pd/ for the generation of the config
sudo apt install acl 
sudo setfacl -Rm user:$USER:rwx /etc/i2pd/

#Create private-key for i2p.
cd i2pd-tools
make
./keygen yovo.dat
mv yovo.dat /etc/i2pd/tunnels.conf.d/

#Enable and start I2P
sudo systemctl enable i2pd
sudo systemctl start i2pd
