******************************
YOVO (You Only Vote Once)
******************************

.. image:: https://gitlab.com/open-yovo/yovo/badges/master/pipeline.svg
   :target: https://gitlab.com/open-yovo/yovo/
   :alt: build
.. image:: https://gitlab.com/open-yovo/yovo/badges/master/coverage.svg
   :target: https://open-yovo.gitlab.io/yovo/html/
   :alt: Coverage
.. image:: https://open-yovo.gitlab.io/yovo/pylint.svg
   :target: https://open-yovo.gitlab.io/yovo/annexes/pylint.html
   :alt: Pylint
.. image:: https://open-yovo.gitlab.io/yovo/vulns.svg
   :target: https://open-yovo.gitlab.io/yovo/vulns.txt
   :alt: Vulns
.. image:: https://img.shields.io/badge/License-MIT-yellow.svg
   :target: https://opensource.org/licenses/MIT
   :alt: License

About
=============
YOVO is decentralized, anonymous (and hopefully secured) voting system based on MultiParty Computation and Homomorphic Encryption. It is built on top of `MP-SPDZ <https://github.com/data61/MP-SPDZ>`_ and `i2pd <https://i2pd.readthedocs.io/en/latest/>`_.

**Important note**: this *school project* is provided as a proof-of-concept. While it is theoretically resilient to a N-1 adversaries scenario, we advise you not use it in production.

Table of contents
=================
While most of the content has been translated from french to english, some parts remain in our mother tongue (especially the risk analysis in the documentation).

- `Miscellaneous documentation (fr/en) <https://open-yovo.gitlab.io/yovo/>`_
- Installing (en): `repository <INSTALL.md#install>`_ / `documentation <general/install.html#install>`_
- Use (en): `repository <src/USAGE.md>`_ / `documentation <general/usage.html>`_
- About the backend (en): `repository <src/BACKEND.md>`_ / `documentation <general/backend.html>`_
- About the frontend (en): `repository <src/FRONTEND.md>`_ / `documentation <general/frontend.html>`_
- Test (en): `repository <INSTALL.md#test>`_ / `documentation <general/install.html#test>`_
- Build the documentation (en): `repository <INSTALL.md#build-the-documentation>`_ / `documentation <general/install.html#build-the-documentation>`_
