# How to use YOVO

## Organisational requirements
Before starting the vote's process, participants must agree on various informations:
- The candidates' list (in order);
- An unique ID for each participants;
- The starting date and hour of the vote.

These have to be shared on a secured, trusted (but not provided by this project) communication channel. It will also be useful during the vote process later on.

## User modes
YOVO can be used in 3 ways:
- Via the web interface, step-by-step;
- Via the web interface, by importing a previous configuration;
- Via the CLI interface.

While the two first points can be viewed as nominal cases, we felt the need to mention the last one as it allows to bypass the browser.

## Using the CLI
![CLI for YOVO](./static/images/cli.png)

You can use YOVO through its Command Line Interface ; simply start the [yovo/cli.py](yovo/cli.py) program and follow the instructions.

## Web interface: an introduction
As long as you want to use the web interface, you'll need to start the web server by using the `src/yovo/start.sh` script. By default, the API and web interface are available on `127.0.0.1:5000`.

The web interface orchestrate the following API calls;
1. `/candidate/change`
2. `/i2p/setup`
3. `/i2p/tunnels`
4. `/vote/start`
5. `/vote/results`

It's made of various pages:
- *Index*: the various forms to use YOVO;
- *How to*: links to this file;
- *About*: links to `about.md`.

The index is sub-divided into multiple sections, available in the horizontal menu:
- *Candidates*: describes the potential values of the vote;
- *I2P & friends*: accepts the local (server) and global (tunnels) I2P configuration;
- *Casting the vote*: used to set the vote's value and the date/hour to start the vote;
- *Results*: displays the vote's results when they are available;
- *Manual configuration*: allows to import/export a vote configuration.

## Web interface: a step-by-step guide
### Adding candidates
Firstly, you need to add the candidates' list used for the vote:

![Candidates' list](./static/images/add_candidate.png)
![Candidates added popup](static/images/candidates_added.png)

Note: you need to enter the candidates in the same order as everyone else to get similar final results.

Here, we discover two important features of the web interface:
- Buttons without border represent direct actions on the web page whereas the ones with a white border corresponds to one or more calls to the API.
- Every API call goes with an information pop up based on the server's return's code and message. It enables you to go to the next section if everything went fine.

### Configuring I2P
The anonymous network's configuration is a two steps process.

First, a local server has to be set up based on your ID (determined beforehand with the rest of the participants) and an arbitrary port number.

![Local I2P configuration](./static/images/i2p_local_config.png)

To be taken into account, you need to restart the i2dp service.

```
systemctl restart i2pd
```

As the displayed message in the web interface implies, you can now get the local server address by check the i2pd's webconsole (by default available on `127.0.0.1:7070`), in the `I2P tunnels` section.

![Getting the local server address in the webconsole](./static/images/i2p_local_red.png)

This address has to be shared with the other vote's participants. While doing so, you need to retrieve theirs and add them in the second part of the web interface, along with their IDs. Once again, the `i2pd` service has to be restarted.

![Adding the tunnels in the web interface](./static/images/i2p_global_tunnels_interface.png)

A visit to the i2pd's webconsole allows you to check the tunnels' creation (`127.0.0.1:7070` > `I2P tunnels` > `Client Tunnels`).

![Confirming the tunnels' creation inside the i2pd's webconsole](./static/images/i2p_global_tunnels_webconsole.png)

Note: the numerical IDs used here have to be unique; various error messages will be displayed if it's not the case.

![Unique ID error message](./static/images/i2p_id_unique.png)

We advise you to wait for awhile (between 20 to 30 minutes) for i2p to set the tunnels between the participants. By the way, the following feature allows just that.

### Casting and scheduling the vote
After setting up the i2p's configuration, you can choose your vote's value in a list based on the candidates you previously entered. You can also choose when the vote will start.

Note: you need to agree on the starting date/time with the other participants beforehand as it's needed for the vote process to correctly start.

### Displaying results
The results are automatically retrieved and displayed in the `Results` section as soon as possible. Please note that a vote can be lengthy; numerous data transmissions are required and doing so over i2p can be long. Eg, a 2 participants vote needs 5 to 10 minutes to end, a 3 participants vote between 10 and 20 minutes.

![Results displayed](./static/images/results.png)

If needed, you can stop the vote process by clicking on the `Stop the requests` button. After it, you'll need to restart the web server.

## Web interface: importing a configuration
To speed up the vote process, it's possible to use the import function. For it, you need to follow a basic JSON format and copy/paste or upload it in the `Manual configuration` section.

Note: this needs a previously set up i2p configuration. This feature is generally used to replay a vote with some already known participants.

That being said, an configuration example is available by click the `Show an example` button.

![Configuration example](./static/images/show_example_config.png)

By correctly editing this one, and then copying/pasting it in the text area (or by uploading from a file), the whole vote process can be started. If the configuration follows the expected standard, you'll only need to wait for the results to be displayed in the relevant section.

## Web interface: exporting the configuration
To help with the replay of votes, an export feature is available below the import one.

It enables you to extract in a reusable format the current configuration used in the web interface. This works either with the step-by-step or import-a-configuration mode. Which means you can use the detailed process via the interface once and then export a functional configuration to play a new vote later on, with the same participants.

![Example of an exported configuration](./static/images/export_config.png)  
