# Front end architecture

This documents aims to describe at a high level the architecture used to develop the front end (read: the web interface) of YOVO.

## Philosophy

During the development process, there were 2 main points taken into account when choosing the tooling or ways to code this solution.
1. The website does NOT have to run on multiple platforms. It is not responsive nor will it work on some obscure version of a forgotten browser (*cough IE cough*). That being said, it has been tested on the latest versions of Firefox and Chromium (respectively 74 and 79).
2. The technology stack has to be as transparent as possible. This means we are using some basic, vanilla, good ol' JavaScript and CSS. No SCSS, no minifying process, no fancy Webpack config, no npm packages overfilling the `node_modules` folder.

Still, these two points combined allow us to code with the latest implementations of the language (for example, with the `import` statement in JavaScript or the `display: grid;` in CSS) while putting aside some network performance issues. Sure, the code is not fully optimised, but it is not like we built some archaic, years 2000 style, GeoCities website (no offence intended to previous GeoCities users).

Also, by using this simplified (because non-existent) frontend pipeline, we provide a greater transparency to the user. The code, freely available, segmented and commented directly in the browser, is the same as the one you will be reading in this repository.

This also means you could possibly modify/debug the JavaScript code here and see the immediate effects in your browser. Which is kinda cool and something modern web development have forgotten.

## HTML Templates

Found in `./templates`.

- The different templates inherit from `base.html`, containing the HTML `header` tag, including all the CSS and JavaScript. Also responsible of the main navigation.
- `index.html` contains the main form, organised by sections (candidates, I2P configuration, casting and results).
- `about.html` explains what this is all about.
- `how-to.html` describes how to use the website for the end users.

## CSS

Found in `./static/assets/css`.

### Organisation

- `./static/assets/css/utils/a11y.css`: some minimal but still existing code to better the accessibility.
- `./static/assets/css/utils/reset.css`: resets/counters the default style of various browsers.
- `./static/assets/css/utils/variables.css`: contains all the custom CSS variables, such has fonts (located in `./static/fonts`), colours, spaces, etc.
- `./static/assets/css/forms.css`: the forms (inputs / buttons) needed a specific file to keep things neat and clear.
- `./static/assets/css/core.css`: imports all the previous stuff. Contains the description of the content and its sections.

### Random notes

We take full advantage of:
- the [`@import` statement](https://developer.mozilla.org/en-US/docs/Web/CSS/@import). Yes, it makes some network overhead, but who cares, we're only using this on localhost. Right? Right, folks?
- custom [CSS properties/variables](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties) for theming purposes (with this beautiful syntax: `var(--some-var)`).

While the website is *not* built to be responsive, we still use `rem` units to respect users' configuration if they ever want to change the default font size.

## JavaScript

Found in `./static/assets/js`.

### Organisation

- `./static/assets/js/core.js`: imports the different components and starts them.
- `./static/assets/js/nav.js`: responsible of the navigation between sections.
- `./static/assets/js/api.js`: centralizes the API calls to be used in the various other files.
- `./static/assets/js/utils.js`: contains helper functions (adding elements to an HTML list, the Popup class) used in the various other files.
- `./static/assets/js/sections/*`: a file for each HTML section found in `./templates/index.html`.

Each section file starts with an exported constant (e.g.: `export const EXAMPLE`) containing a start function responsible of:
- setting all the UI elements;
- setting various logic variables used later in the code;
- starting the potential events listeners.

This allows us to import said constant and simply call `EXAMPLE.start()`, while keeping the relevant code clean and in a separated file.

### A more visual approach

The following drawing represents the architecture used for every file found in the section folder:

![JavaScript architecture schema](./static/images/frontend_js_schema.png)

### API calls

The API calls made in the sections files use the `./static/assets/js/api.js`'s `callApi` function. This helper is parametrised to use the `localhost:5000` default endpoint.

### The Popup class

To communicate relevant information to the user, a custom class have been created. Located in the `./static/assets/js/utils.js`, it uses an HTML element to display a custom message with a title, a number of HTML elements (eg: some paragraphs) and a state (`error`, `success`).

For example, it is used as an success/error callback for API calls or when some wrong configuration is detected.

### A note on saving data
During the late stages of development, a question arose: if we want to provide a way for the user to recover its configuration, where should it be saved?

A first approach is to use the `localStorage` API (see the [MDN documentation](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage)), which provides a clean and easy way to save complex data as a *stringified* JSON, persisting between browsers sessions. While its appeal is undeniable, the last part could be worrisome.

Of course, we know the vote's data will stay on the file system (depending on whether the user is currently using a live USB key or not) but reducing the potential attack surface is always good.

Which is why the `localStorage` API was replaced by its less elegant counterpart: saving the data inside a global variable in the `window` object. We can still manage the votes' state and data, but every time the browser sessions ends, this sensitive data is virtually wiped.

## Changing the host/port
If for some obscure reason you changed the default host and port of the API and want the UI to be able to contact it, you need to modify the `web/static/assets/js/api.js` file.

```
export function callApi(method, endpoint, data = '', successCallback, errorCallback) {
  let host = '127.0.0.1'
  let port = '5000'
  // [...]
}
```
