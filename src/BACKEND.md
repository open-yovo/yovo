# Back end architecture

This documents aims to describe at a high level the architecture used to develop the back end of YOVO.

## Philosophy

During the development we divided the work in two phases :
1. Build vote logic and make in functional;
2. Add a web based user interface which interacts with the logic through an API and I2P logic.

Knowing we had to make our phase development phase modular and easy to interface with an (at the moment) unknown UI part, we decided to use files as the main way to interact with the vote's logic. We use a configuration object with all paths to the files to be read during the execution, which means those files can be generated independently by an other program and read later.

Then, the API part is mainly about ordering calls to the vote's logic and write required data in appropriated files.

Here is our Python file tree:
```
.
└── yovo
    ├── config
    │   ├── candidates-exec.json
    │   ├── hosts-exec.json
    │   ├── paths.json
    │   ├── hosts-tunnels-exec.json
    │   ├── vote-exec.json
    │   ├── vote.mpc.jinja2
    │   ├── yovo-server.config.jinja2
    │   └── yovo-tunnel.config.jinja2
    ├── output
    │   └── ...
    ├── main.py
    ├── candidate.py
    ├── host.py
    ├── interface.py
    ├── utils.py
    └── vote.py
```

We will explore these files functions in the following parts.

## Flask

We decided to use Flask as an API engine as it's the easiest to use in Python and it's really lightweight. We defined 5 API endpoints and 1 UI endpoint :
- `/`: handles the WEB UI code;
- `/i2p/setup` : initializes the vote and the hidden service for i2p.
- `/i2p/tunnels` : initializes the i2p tunnels to other voters.
- `/candidate/change`: configures the candidates' list;
- `/vote/start`: starts the vote at a defined date/time along with the specified vote;
- `/vote/results`: reads the vote's results.

The API code found at `main.py` is interfaced with the voting system through the `Interface` class in `interface.py`. This basically instantiates a vote, gets JSON data from the API and passes it to the vote or write it in the relevant file while verifying that calls to the vote instance are done in the appropriate order.

## Vote logic
To produce a decentralized vote, we use the MP_SPDZ framework which enables us to compute a secured, multiparty addition. From that, we can deduct the vote's result (read the complete documentation to read more about that).
We devided the code in 3 main parts :
- `vote.py`: conducts the main vote's logic;
- `candidate.py`: implements the candidates' list;
- `host.py`: handles the I2P configuration generation and MP_SPDZ hosts' configuration.

### Vote steps
The Vote class at `vote.py` implements multiple steps to correctly run a vote :
1. Initializes the vote with required parameters (paths configuration, party ID and starting port);
2. Initializes the tunnels to the other voters ( with their party id and i2p addresses)
2. Enters a candidates list;
3. Enters the vote's value;
4. Prepares the vote (checks the vote parameters, compiles the MPC program, encodes the vote value);
5. Runs the vote thanks to the MP_SPDZ framework;
6. Reads the result and decode it to print it in a human readable format.

These steps are followed by the `Interface` object at `interface.py` as described previously.

### A more visual approach

The following drawing represents the architecture used for main backek end files:

![Back end architecture schema](./static/images/backend_schema.png)

## Configuration and output files

Here is a quick tour of the files we use as configuration and output.

Configuration:
- `config/candidates-exec.json`: contains candidate's list;
- `config/hosts-exec.json`: contains the configuration for the hidden service (party id and starting port)
- `config/hosts-tunnels-exec.json` : contains the configuration for the i2p tunnels to the other voters (party id and i2p addresses)
- `config/vote-exec.json`: contains the vote's value (ID of the chosen candidate);
- `config/vote.mpc.jinja2`: MP_SPDZ program template to be run to execute a vote;
- `config/yovo-server.config.jinja2`: I2P server configuration template;
- `config/yovo-tunnel.config.jinja2`: I2P tunnel configuration template.
- `config/paths.json` : list of filepaths used for the project.

Output:
- `output/vote.mpc`: MP_SPDZ voting program compiled;
- `output/ballot` binary result retrieved from MP_SPDZ;
- `output/result.json`: processed result from the binary file with the number of votes by candidate.


## Ressource files organisation 

All ressource files containing either configuration information or json sampe for tests usage are centralized in the `/config/path.json` file.

The advantage of this access scheme is to allow easier reorganization for those file even if they are accessed by several differents part of our program. 

TODO : insert dank schema

In order to avoid dead example or badly formulated json all the path stored in this file are checked upon running `pytest`. This test includes : 

- **Existence**: files that are documented but do not exist on the file system raise an error. 
- **Valid json**: files path that end with a .json but refers to badly formulated json raise an error. 

Files whose path contains "-exec" are not checked for existence. The goal of this exception is to allow referencing for file present on the file system only at runtime. This option is opt-in rather than opt-out because it's usage increase the risk of having dead examples. You should therefore use it with parsimony. 


