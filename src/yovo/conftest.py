"""
Create Flask application to run API tests
"""

import pytest

from yovo.main import create_app
from yovo.utils import get_config


@pytest.fixture
def app():
    """
    Use Pytest-flask to use an API cient in tests
    """

    config = get_config()
    config["output_i2p"] = "output/i2p_output/"
    application = create_app(config=config)
    application.debug = True
    return application
