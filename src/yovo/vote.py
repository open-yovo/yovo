"""
This module describe the core of out voting system
"""

import os
import json
import logging
import shutil

from jinja2 import Template

from yovo.host import HostList
from yovo.candidate import CandidateList
from yovo.utils import (
    handle_file_open_error,
    handle_file_write_error,
    write_json,
    execute_command,
    get_config,
    BALLOT_MAX_SIZE,
)

logger = logging.getLogger("yovo")


class Vote:  # pylint: disable=too-many-instance-attributes
    """
    Define a vote process from host input to result display

    Attributes:
        host_list: :obj:`host.HostList`
            Hosts list
        voters_nb: :obj:`int`
            Number of voters
        party_id: :obj:`int`
            ID used to run MP SPDZ as party number
        result: :obj:`dict`
            Result of vote
        ballot: :obj:`int`
            Brut result of vote
        candidate_list: :obj:`candidate.CandidateList`
            List of candidates
        config: :obj:`dict`
            Configuration to list all path to files
    """

    # pylint: disable=bad-continuation
    def __init__(self, config=get_config()):  # pylint: disable=dangerous-default-value

        logger.info("[VOTE] Instantiating vote object")
        if not all(
            field in config
            for field in [
                "host-server",
                "host-tunnels",
                "candidates",
                "program",
                "compiled",
                "ballot",
                "vote",
                "result",
            ]
        ):
            raise ValueError(
                "Configuration provided isn't in the required format check all fields are present",
            )
        self.config = config

        self._party_id = None
        self._voters_nb = None
        self.host_list = HostList(
            config["template_server"],
            config["template_tunnel"],
            config_server_path=config["host-server"],
        )
        self.candidate_list = CandidateList(config["candidates"])
        self.ballot = 0
        self.result = {}

    @property
    def voters_nb(self):
        """ Property to save number of voters. """

        return self._voters_nb

    @voters_nb.setter
    def voters_nb(self, voters_nb):
        """ Property setter to save number of voters. """

        logger.info(f"[VOTE] Setting voters number from {self._voters_nb} to {voters_nb}")
        if voters_nb <= 2:
            raise ValueError("Voters number is insufficient must be > 2")
        self._voters_nb = voters_nb

    @property
    def party_id(self):
        """ Property to save party ID. """

        return self._party_id

    @party_id.setter
    def party_id(self, party_id):
        """ Property setter to save party ID. """

        logger.info(f"[VOTE] Setting party ID from {self._party_id} to {party_id}")
        self._party_id = party_id

    def generate_i2p_server(self):
        """
        generate hidden service config file for i2p
        """

        self.host_list.generate_i2p_server(self.config["output_i2p"])

    def generate_i2p_tunnels(self):
        """
        generate the tunnels config files for i2p and get number of voters from that
        """

        self.voters_nb = self.host_list.load_tunnels_config(self.config["host-tunnels"]) + 1
        self.host_list.generate_i2p_tunnels(self.config["output_i2p"])

    def check_configuration(self):
        """
        Check if party_id and the number of voters are compatible, raise an error if not
        """

        logger.info(
            f"[VOTE] Checking party ID is in the required interval ([0, {self._voters_nb}[)"
        )
        if not 0 <= self.party_id < self._voters_nb:
            raise ValueError("The party_id provided isn't compatible with the number of voters")

    def compile_program(self):
        """
        Replace variables in vote.mpc and write it in output
        """

        logger.info("[VOTE] Starting vote compilation")
        nb_voters = self.voters_nb

        try:
            logger.debug("[VOTE] Reading program template")
            program_str = ""
            with open(self.config["program"], "r", encoding="utf-8") as program:
                program_str = program.read()

            logger.debug("[VOTE] Rendering tempalte with number of voters")
            template = Template(program_str)
            output = template.render(nb_voters=nb_voters)

            logger.debug("[VOTE] Writing compiled template to output directory")
        except IOError as exception:
            handle_file_open_error(exception, self.config["program"])

        try:
            with open(self.config["compiled"], "w", encoding="utf-8") as compiled:
                compiled.write(output)
        except IOError:
            handle_file_write_error(self.config["compiled"])

    def encode_vote(self):
        """
        Take vote from config file, transform it into appropiated payload according to
        number of voters and write it in output folder.
        """

        logger.info(
            "[VOTE] Encoding raw vote (candidate ID) to an integer which will be added to balot"
        )
        if self.party_id is None:
            raise Exception("Party ID is not set")
        path_input_file = f"{self.config['output_dir']}Input-P{self.party_id}-0"
        with open(path_input_file, "w", encoding="utf-8") as input_file:
            with open(self.config["vote"], "r", encoding="utf-8") as vote_file:
                vote = int(json.load(vote_file)["vote"])
                # padding calculated from the number of digit required to store votes
                # for a candidate on the ballot this padding is used to
                # divide the ballot to count nex candidate vote count
                logger.debug("[VOTE] Calculating vote integer")
                padding = pow(10, int(self.voters_nb / 10) + 1)
                computed_vote = pow(padding, vote)
                logger.debug(f'[VOTE] Writing encoded result to "{path_input_file}"')
                input_file.write(str(computed_vote))

    def read_ballot(self):
        """
        Read ballot from binary file and decode it to decimal value
        """

        logger.info("[VOTE] Reading vote ballot from binary file")
        try:
            with open(self.config["ballot"], "rb") as result_file:
                self.ballot = int.from_bytes(result_file.read(8), byteorder="little")
        except IOError as exception:
            handle_file_open_error(exception, self.config["ballot"])

    def check_ballot_max_size(self):
        """
        Check ballot won't exceed maximul ballot size supported
        """

        logger.info("[VOTE] Checking final ballot size won't exceed maximum size available")
        padding = pow(10, int(self.voters_nb / 10) + 1)
        nb_candidate = self.candidate_list.candidates_nb
        max_size = pow(padding, nb_candidate - 1) * self.voters_nb
        if max_size >= BALLOT_MAX_SIZE:
            raise ValueError(
                "Ballot potential max size is superior to system allowed max ballot size"
            )

    def run_program(self):
        """
        Run program with MP SPDZ
        """

        logger.info("[VOTE] Start vote process through MP-SPDZ")
        if self.party_id is not None:
            # transfer files to MP SPDZ
            logger.debug("[VOTE] Removing potential old vote output if it exists")
            if os.path.isfile(f"../../mp_spdz/Player-Data/Private-Output-{self.party_id}"):
                os.remove(f"../../mp_spdz/Player-Data/Private-Output-{self.party_id}")

            logger.debug("[VOTE] Transfering vote program to MP-SPDZ directory")
            shutil.copy(self.config["compiled"], "../../mp_spdz/Programs/Source/vote.mpc")
            logger.debug("[VOTE] Transfering encoded vote to MP-SPDZ player data")
            shutil.copy(
                f"output/Input-P{self.party_id}-0",
                f"../../mp_spdz/Player-Data/Input-P{self.party_id}-0",
            )
            logger.debug("[VOTE] Moving to MP-SPDZ directory for execution")
            os.chdir("../../mp_spdz")
            # execute MP SPDZ program
            logger.debug("[VOTE] Compiling vote program to .mpc file")
            execute_command(["./compile.py", "vote", "-R 64"])
            logger.debug("[VOTE] Generating host file for MP-SPDZ")
            self.host_list.generate_mpspdz_config("./hosts.txt")
            logger.debug("[VOTE] Starting MP-SPDZ")
            execute_command(
                [
                    "./spdz2k-party.x",
                    "-ip hosts.txt",
                    f"-N {self.voters_nb}",
                    f"-p {str(self.party_id)}",
                    "vote",
                ]
            )

            # get result from MP SPDZ
            logger.debug("[VOTE] Getting result from MP-SPDZ")
            if not os.path.isfile(f"Player-Data/Private-Output-{self.party_id}"):
                os.chdir("../src/yovo/")
                raise Exception("MP-SPDZ didn't ended correctly, can't retrieve ballot result")
            shutil.copy(
                f"Player-Data/Private-Output-{self.party_id}",
                f'../src/yovo/{self.config["ballot"]}',
            )
            logger.debug("[VOTE] Moving back to YOVO directory for execution")
            os.chdir("../src/yovo/")
        else:
            raise Exception("Party ID isn't set")

    def decode_ballot(self):
        """
        Decode ballot to get number of votes by candidate ID
        """

        logger.info("[VOTE] Decoding vote result to get number of vote by candidate ID")
        # sum of all voters vote
        ballot = self.ballot
        # padding calculated from the number of digit required to store votes
        # for a candidate on the ballot this padding is used to
        # divide the ballot to count nex candidate vote count
        padding = pow(10, int(self.voters_nb / 10) + 1)
        id_candidate = 0
        total = 0

        # decoding of the ballot
        for _ in range(self.candidate_list.candidates_nb):
            cpt = 0
            while ballot % padding != 0:
                cpt += 1
                ballot -= 1
            ballot = ballot / padding
            logger.debug(f"[VOTE] Candidate {id_candidate} had {cpt} votes")
            self.result[id_candidate] = cpt
            total += cpt
            id_candidate += 1

        logger.debug("[VOTE] Checking votes are compliant with number of voters and candidates")
        if total > self.voters_nb:
            raise ValueError("Total vote number is superior than voters number")
        if ballot > 0:
            raise ValueError("Number of candidates calculated from ballot is too high")

    def map_result(self):
        """
        Map candidate Name with there score in the vote and write it into output file
        """

        logger.info("[VOTE] Mapping candidate ID with candidate name")
        mapped_result = {}
        # map vote count with candidate display by joining with candidate ID
        for id_candidate in range(len(self.candidate_list.candidates)):
            candidate = self.candidate_list.candidates[id_candidate]
            nb_votes = 0
            if id_candidate in self.result:
                nb_votes = self.result[id_candidate]
                mapped_result[candidate] = nb_votes
            else:
                mapped_result[candidate] = nb_votes
            logger.debug(f'[VOTE] Candidate "{candidate}" had {nb_votes} votes')
        write_json(mapped_result, self.config["result"])
