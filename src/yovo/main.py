"""
Manage flask API creation and calls to interface dispatch
"""
import os
import signal
import logging

from flask import Flask, render_template, request, abort, send_from_directory

from yovo.interface import Interface
from yovo.utils import get_config, setup_logging

logger = setup_logging()


def create_app(config=get_config()):  # pylint: disable=dangerous-default-value
    """ Instantiate a Flask application with all defined endpoints. """

    app = Flask(__name__, static_folder="./../static", template_folder="./../templates")
    logging.getLogger("werkzeug").disabled = True

    interface = Interface(config=config)

    @app.before_request
    def before_request_func():  # pylint: disable=unused-variable
        logger.info(f"[FLASK]: {request.method} - {request.full_path}")

    @app.after_request
    def after_request(response):  # pylint: disable=unused-variable
        header = response.headers
        header["Access-Control-Allow-Origin"] = "*"
        return response

    @app.route("/favicon.ico")
    def favicon():  # pylint: disable=unused-variable
        return send_from_directory(
            "../static/images/", "icone.ico", mimetype="image/vnd.microsoft.icon"
        )

    # ===============================================#
    #               Templates routes                 #
    # ===============================================#

    @app.route("/index")
    @app.route("/")
    def index():  # pylint: disable=unused-variable
        return render_template("index.html")

    @app.route("/about")
    def about():  # pylint: disable=unused-variable
        return render_template("about.html")

    @app.route("/how-to")
    def how_to():  # pylint: disable=unused-variable
        return render_template("how-to.html")

    # ===============================================#
    #                  API routes                    #
    # ===============================================#

    @app.route("/i2p/setup", methods=["POST"])
    def i2p_setup():  # pylint: disable=unused-variable
        """
        Setup I2P hidden service and instantiate vote

        POST:
            data: :obj:`dict`
                I2P config in json as::
                {
                        "party_id":2,
                        "starting_port":6000,
                }
        """

        try:
            data = request.get_json(force=True)
            interface.setup_i2p_server(data)
            return {}
        except Exception as ex:  # pylint: disable=broad-except
            abort(500, ex)

    @app.route("/i2p/tunnels", methods=["POST"])
    def i2p_tunnels():  # pylint: disable=unused-variable
        """
        Setup I2P config for tunnels

        POST:
            data: :obj:`dict`
                I2P config in json as::
                        {
                            "1": "addr1.b32.i2p",
                            "3": "addr3.b32.i2p",
                            "4": "addr4.b32.i2p",
                        }
        """
        try:
            data = request.get_json(force=True)
            interface.setup_vote(data)
            return {}
        except Exception as ex:  # pylint: disable=broad-except
            abort(500, ex)

    @app.route("/candidate/change", methods=["POST"])
    def candidate_change():  # pylint: disable=unused-variable
        """
        Setup candidate list for the vote

        POST:
            data: :obj:`list`
                Json list of candidates as::

                    ["Patate", "Jean-René de La Motte", "张伟"]
        """

        data = request.get_json()
        interface.setup_candidates(data)
        return {}

    @app.route("/vote/start", methods=["POST"])
    def vote_start():  # pylint: disable=unused-variable
        """
        Start vote at desired time

        POST:
            data: :obj:`dict`
                Start config as::

                    {"vote":"<candid_id>", "date": "24/03/2020 10:32:00"}
                    OR
                    {"vote":"<candid_id>","date": ""}
        """

        try:
            data = request.get_json()
            interface.prepare_vote(data)
            return {}
        except Exception as ex:  # pylint: disable=broad-except
            abort(500, ex)

    @app.route("/vote/results")
    def vote_result():  # pylint: disable=unused-variable
        """
        Request vote results if they exist

        GET:
            data: :obj:`dict`

        Returns:
            Results as::

                {"Robert": 1, "Jean": 1, "René": 2}
        """

        return interface.read_results()

    @app.route("/vote/stop")
    def vote_stop():  # pylint: disable=unused-variable
        """
        Stop the serveur
        """

        logger.info("[FLASK] Killing web server process")
        os.kill(os.getpid(), signal.SIGTERM)

    return app
