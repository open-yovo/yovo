"""
Define some usefull functions and variables used in other modules
"""

import errno
import os
import json
import logging

logger = logging.getLogger("yovo")


class StateException(Exception):
    """State error when calling interface in wrong order."""


def setup_logging():
    """
    Setup logging for YOVO
    """

    logger_tmp = logging.getLogger("yovo")
    logger_tmp.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        "[%(asctime)s][%(levelname)s] %(message)s", datefmt="%d/%m/%Y %H:%M:%S"
    )
    file_handler = logging.FileHandler(filename="yovo.log", encoding="utf-8", mode="w")
    file_handler.setLevel(logging.DEBUG)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)
    console_handler.setFormatter(formatter)
    logger_tmp.addHandler(file_handler)
    logger_tmp.addHandler(console_handler)
    return logger_tmp


def execute_command(command):
    """
    Execute a command in host shell
    """

    logger.info(f'[UTILS] Executing command on system : "{command}"')
    os.system(" ".join(command))


def handle_file_open_error(exception, path):
    """
    An helper used for file open access checks, raising the correct errors

    Parameters:
        exception: :obj:`error`
            The error thrown
        path: :obj:`str`
            The file path responsible of the error
    """

    if exception.errno == errno.EACCES:
        logger.error(f'[UTILS] Error while accessing file at "{path}" : no required permission')
        raise IOError(
            path, "You do not have the required permission to interract with the file.",
        )
    if exception.errno == errno.ENOENT:
        logger.error(f'[UTILS] Error while accessing file at "{path}" : file does not exist')
        raise IOError(
            path, "The file you are trying to access does not exist.",
        )


def handle_file_write_error(path):
    """
    An helper used for file write access checks, raising the correct errors

    Parameters:
        path: :obj:`str`
            The file path responsible of the error
    """

    logger.error(f'[UTILS] Error while writing to file at "{path}"')
    raise IOError(
        path, "You do not have the required permission to interract with the file.",
    )


def write_json(json_object, path):
    """
    Write json object in a file

    Parameters:
        json_object: :obj:`dict`
            Object to write
        path: :obj:`str`
            Path to the file where we want to write the object
    """

    logger.debug(f'[UTILS] Writing JSON object {json_object} to file "{path}"')
    try:
        with open(path, "w", encoding="utf8") as file:
            json.dump(json_object, file, ensure_ascii=False)
    except IOError:
        handle_file_write_error(path)


def read_json(path):
    """
    Read json object in a file

    Parameters:
        path: :obj:`str`
            Path to the file cotnaining the json object
    Return:
        retour: :obj:`str`
            Json data avalable in path
    """

    logger.info(f'[UTILS] Reading JSON from file at "{path}"')
    try:
        with open(path, "r", encoding="utf8") as file:
            return json.load(file)
    except IOError as exception:
        handle_file_open_error(exception, path)


def validate_paths(path):
    """
    Test that the paths of the paths.json files does exists.
    """

    logger.info("[UTILS] Validating paths config file")
    paths = read_json(path)
    for _, subpaths in paths.items():
        for _, file_path in subpaths.items():
            # file that contain -exec in their name are created at execution
            # hence they dont exist on disk during the test.
            if ("-exec" in file_path) or ("-invalid" in file_path):
                continue
            key, extension = os.path.splitext(file_path)  # pylint: disable=unused-variable
            if extension == ".json":
                # if the file is json test it's validity
                try:
                    read_json(file_path)
                except ValueError:
                    raise ValueError(path, "Badly formed json")
            else:
                if not os.path.isdir(file_path) and "/etc/" not in file_path:
                    # and atleast test that the path existes
                    with open(file_path, "r", encoding="utf8"):
                        pass


def get_config():
    """
    Return default configuration for YOVO run
    """

    paths = read_json("config/paths.json")
    paths_config = paths["config"]
    paths_output = paths["output"]

    config = {
        "host-server": paths_config["host-server"],
        "host-tunnels": paths_config["host-tunnels"],
        "candidates": paths_config["candidate"],
        "program": paths_config["program"],
        "compiled": paths_output["compiled_vote"],
        "template_server": paths_config["template_server"],
        "template_tunnel": paths_config["template_tunnel"],
        "vote": paths_config["vote"],
        "ballot": paths_output["ballot"],
        "result": paths_output["result"],
        "output_i2p": paths_output["i2p_output_dir"],
        "output_dir": paths_output["output_dir"],
    }

    return config


BALLOT_MAX_SIZE = 17293822569102704640
AUTO_RESTART = True
