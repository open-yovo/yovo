"""
All tests for vote.py
"""
# pylint: disable=missing-function-docstring,attribute-defined-outside-init,no-self-use,too-many-public-methods

import os
import filecmp
import sys

import pytest

from yovo.vote import Vote
from yovo.utils import read_json


class TestVote:
    """
    Test suit for Vote object
    """

    def setup_class(self):
        """
        class specific setup
        """
        # Global path file handling
        self.paths = read_json("config/paths.json")
        self.config = {
            "host-server": self.paths["host"]["sample_server_config"],
            "host-tunnels": self.paths["host"]["sample_tunnels_config"],
            "candidates": self.paths["candidate"]["sample_candidate_3"],
            "program": "samples/vote_1.mpc",
            "compiled": "output/vote.mpc",
            "ballot": "samples/ballot",
            "vote": self.paths["vote_code"]["sample_vote_1"],
            "result": "output/result.json",
            "output_dir": "output/",
            "template_server": self.paths["host"]["sample_yovo-server-template"],
            "template_tunnel": self.paths["host"]["sample_yovo-tunnel-template"],
            "output_i2p": "output",
        }
        if not os.path.isdir("output"):
            os.makedirs("output")

    ################################
    # init
    ################################
    def test_init_nominal(self):
        vote = Vote(config=self.config)
        vote.voters_nb = 4
        assert vote.voters_nb == 4
        assert vote.result == {}
        assert vote.ballot == 0

    def test_init_empty_config(self):
        with pytest.raises(ValueError):
            Vote(config={})

    def test_init_missing_one_config_field(self):
        tmp_config = self.config.copy()
        del tmp_config["candidates"]
        with pytest.raises(ValueError):
            Vote(config=tmp_config)

    ################################
    # set_voters_nb
    ################################
    def test_voter_nb_nominal(self):
        vote = Vote(config=self.config)
        vote.voters_nb = 3
        assert vote.voters_nb == 3

    def test_voter_nb_two(self):
        vote = Vote(config=self.config)
        with pytest.raises(ValueError):
            vote.voters_nb = 2

    def test_voter_nb_one(self):
        vote = Vote(config=self.config)
        with pytest.raises(ValueError):
            vote.voters_nb = 1

    def test_voter_nb_negative(self):
        vote = Vote(config=self.config)
        with pytest.raises(ValueError):
            vote.voters_nb = -3

    ################################
    # set_party_id
    ################################
    def test_set_party_id_nominal(self):
        vote = Vote(config=self.config)
        vote.party_id = 2
        vote.voters_nb = 4
        vote.check_configuration()
        assert vote.party_id == 2

    def test_set_party_id_negative(self):
        vote = Vote(config=self.config)
        vote.party_id = -1
        vote.voters_nb = 4
        with pytest.raises(ValueError):
            vote.check_configuration()

    def test_set_party_id_to_high(self):
        vote = Vote(config=self.config)
        vote.party_id = 4
        vote.voters_nb = 4
        with pytest.raises(ValueError):
            vote.check_configuration()

    ################################
    # compile_program
    ################################
    def test_compile_program_nominal(self):
        config = self.config.copy()
        config["program"] = self.paths["vote"]["sample_vote_1"]
        vote = Vote(config=config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        vote.compile_program()
        assert filecmp.cmp(config["compiled"], self.paths["vote"]["sample_vote_1_ref"])

    def test_compile_program_empty(self):
        config = self.config.copy()
        config["program"] = self.paths["vote"]["sample_vote_2"]
        vote = Vote(config=config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        vote.compile_program()
        assert filecmp.cmp(config["compiled"], self.paths["vote"]["sample_vote_2_ref"])

    def test_compile_program_without_keyword_nb_voters(self):
        config = self.config.copy()
        config["program"] = self.paths["vote"]["sample_vote_3"]
        vote = Vote(config=config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        vote.compile_program()
        assert filecmp.cmp(config["compiled"], self.paths["vote"]["sample_vote_3_ref"])

    def test_compile_program_no_program_file(self):
        config = self.config.copy()
        config["program"] = "./bla.txt"
        vote = Vote(config=config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        with pytest.raises(IOError):
            vote.compile_program()

    def test_compile_program_cant_write_program(self):
        config = self.config.copy()
        config["program"] = self.paths["vote"]["sample_vote_1"]
        config["compiled"] = "not_a_directory/output.mpc"
        vote = Vote(config=config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        with pytest.raises(IOError):
            vote.compile_program()

    ################################
    # encode_vote
    ################################
    def test_encode_vote_nominal(self):
        config = self.config.copy()
        config["vote"] = self.paths["vote_code"]["sample_vote_1"]
        vote = Vote(config=config)
        vote.party_id = 1
        vote.voters_nb = 4
        vote.check_configuration()
        vote.encode_vote()
        assert filecmp.cmp("output/Input-P1-0", self.paths["vote_code"]["sample_vote_1_ref"])

    def test_encode_vote_candidate_0(self):
        config = self.config.copy()
        config["vote"] = self.paths["vote_code"]["sample_vote_2"]
        vote = Vote(config=config)
        vote.party_id = 1
        vote.voters_nb = 4
        vote.check_configuration()
        vote.encode_vote()
        assert filecmp.cmp("output/Input-P1-0", self.paths["vote_code"]["sample_vote_2_ref"])

    def test_encode_vote_candidate_max(self):
        config = self.config.copy()
        config["vote"] = self.paths["vote_code"]["sample_vote_3"]
        vote = Vote(config=config)
        vote.party_id = 1
        vote.voters_nb = 4
        vote.check_configuration()
        vote.encode_vote()
        assert filecmp.cmp("output/Input-P1-0", self.paths["vote_code"]["sample_vote_3_ref"])

    def test_encode_vote_no_party_id(self):
        vote = Vote(config=self.config)
        with pytest.raises(Exception):
            vote.encode_vote()

    ################################
    # read_ballot
    ################################
    def test_read_ballot_nominal(self):
        config = self.config.copy()
        config["ballot"] = self.paths["ballot"]["sample_ballot_nominal"]
        vote = Vote(config=config)
        vote.read_ballot()
        assert vote.ballot == 200

    def test_read_ballot_file_doesnt_exists(self):
        config = self.config.copy()
        config["ballot"] = "sample_does_not_exist"
        vote = Vote(config=config)
        with pytest.raises(IOError):
            vote.read_ballot()

    ################################
    # check_ballot_max_size
    ################################
    def test_check_ballot_max_size_nominal(self):
        vote = Vote(config=self.config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        vote.check_ballot_max_size()

    def test_check_ballot_max_size_too_much_voter(self):
        vote = Vote(config=self.config)
        vote.party_id = 0
        vote.voters_nb = 10000
        vote.check_configuration()
        with pytest.raises(ValueError):
            vote.check_ballot_max_size()

    def test_check_ballot_max_size_too_much_candidate(self):
        config = self.config.copy()
        config["candidates"] = self.paths["candidate"]["sample_candidate_4"]
        vote = Vote(config=config)
        vote.party_id = 0
        vote.voters_nb = 100
        vote.check_configuration()
        with pytest.raises(ValueError):
            vote.check_ballot_max_size()

    ################################
    # run_program
    ################################
    def test_run_program_nominal(self, mocker):
        with open("../../mp_spdz/Player-Data/Private-Output-0", "w") as tmp_file:
            tmp_file.write("tmp_data")

        vote = Vote(config=self.config)
        vote.party_id = 0
        vote.generate_i2p_tunnels()
        mock_system = mocker.patch("os.system")
        mock_remove = mocker.patch("os.remove")
        mock_copy = mocker.patch("shutil.copy")

        vote.run_program()

        expected_copy_calls = [
            mocker.call(self.config["compiled"], "../../mp_spdz/Programs/Source/vote.mpc"),
            mocker.call("output/Input-P0-0", "../../mp_spdz/Player-Data/Input-P0-0"),
            mocker.call("Player-Data/Private-Output-0", "../src/yovo/samples/ballot"),
        ]
        mock_copy.assert_has_calls(expected_copy_calls)

        expected_system_calls = [
            mocker.call("./compile.py vote -R 64"),
            mocker.call("./spdz2k-party.x -ip hosts.txt -N 4 -p 0 vote"),
        ]
        mock_system.assert_has_calls(expected_system_calls)

        expected_remove_calls = [mocker.call("../../mp_spdz/Player-Data/Private-Output-0")]
        mock_remove.assert_has_calls(expected_remove_calls)

        mocker.stopall()
        os.remove("../../mp_spdz/Player-Data/Private-Output-0")

    def test_run_program_no_party_id(self, mocker):

        vote = Vote(config=self.config)
        vote.generate_i2p_tunnels()

        mock_system = mocker.patch("os.system")
        mock_remove = mocker.patch("os.remove")
        mock_copy = mocker.patch("shutil.copy")

        with pytest.raises(Exception):
            vote.run_program()

        assert not mock_copy.called
        assert not mock_system.called
        assert not mock_remove.called

        mocker.stopall()

    def test_run_program_mpspdz_crashed(self, mocker):

        vote = Vote(config=self.config)
        vote.party_id = 0
        vote.generate_i2p_tunnels()

        mock_system = mocker.patch("os.system")
        mock_remove = mocker.patch("os.remove")
        mock_copy = mocker.patch("shutil.copy")

        with pytest.raises(Exception):
            vote.run_program()

        expected_copy_calls = [
            mocker.call(self.config["compiled"], "../../mp_spdz/Programs/Source/vote.mpc"),
            mocker.call("output/Input-P0-0", "../../mp_spdz/Player-Data/Input-P0-0"),
        ]
        mock_copy.assert_has_calls(expected_copy_calls)

        expected_system_calls = [
            mocker.call("./compile.py vote -R 64"),
            mocker.call("./spdz2k-party.x -ip hosts.txt -N 4 -p 0 vote"),
        ]
        mock_system.assert_has_calls(expected_system_calls)

        assert not mock_remove.called

        mocker.stopall()

    ################################
    # decode_ballot
    ################################
    def test_decode_ballot_nominal(self):
        vote = Vote(config=self.config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        vote.ballot = 211
        vote.decode_ballot()
        assert vote.result[0] == 1
        assert vote.result[1] == 1
        assert vote.result[2] == 2

    def test_decode_ballot_more_candidate_than_registered(self):
        vote = Vote(config=self.config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        vote.ballot = 1111
        with pytest.raises(ValueError):
            vote.decode_ballot()

    def test_decode_ballot_less_candidate_than_registered(self):
        vote = Vote(config=self.config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        vote.ballot = 22
        vote.decode_ballot()
        assert vote.result[0] == 2
        assert vote.result[1] == 2
        assert vote.result[2] == 0

    def test_decode_ballot_null_value(self):
        vote = Vote(config=self.config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        vote.ballot = 0
        vote.decode_ballot()
        assert vote.result[0] == 0
        assert vote.result[1] == 0
        assert vote.result[2] == 0

    def test_decode_ballot_less_vote_than_nb_voters(self):
        vote = Vote(config=self.config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        vote.ballot = 111
        vote.decode_ballot()
        assert vote.result[0] == 1
        assert vote.result[1] == 1
        assert vote.result[2] == 1

    def test_decode_ballot_more_vote_than_nb_voters(self):
        vote = Vote(config=self.config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        vote.ballot = 131
        with pytest.raises(ValueError):
            vote.decode_ballot()

    ################################
    # map_result
    ################################
    def test_map_result_nominal(self):
        vote = Vote(config=self.config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        vote.result = {0: 1, 1: 1, 2: 2}
        vote.map_result()
        assert filecmp.cmp(self.config["result"], self.paths["result"]["sample_result_ref_1"])

    def test_map_result_less_candidate_than_registered(self):
        vote = Vote(config=self.config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        vote.result = {0: 1, 1: 2}
        vote.map_result()
        assert filecmp.cmp(self.config["result"], self.paths["result"]["sample_result_ref_2"])

    def test_map_result_null_value(self):
        vote = Vote(config=self.config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        vote.result = {}
        vote.map_result()
        assert filecmp.cmp(self.config["result"], self.paths["result"]["sample_result_ref_3"])

    def test_map_result_more_candidate_than_registered(self):
        vote = Vote(config=self.config)
        vote.party_id = 0
        vote.voters_nb = 4
        vote.check_configuration()
        vote.result = {0: 1, 1: 1, 2: 2, 3: 1}
        vote.map_result()
        assert filecmp.cmp(self.config["result"], self.paths["result"]["sample_result_ref_4"])


if __name__ == "__main__":
    print("This file should be used through py.test")
    sys.exit(1)
