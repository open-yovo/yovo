"""
All tests for host.py
"""
# pylint: disable=missing-function-docstring,attribute-defined-outside-init,no-self-use

import filecmp
import os
import sys

import pytest

from yovo.host import HostList
from yovo.utils import read_json


class TestHostList:  # pylint: disable=too-many-instance-attributes
    """
    Test suite for HostList object
    """

    def setup_class(self):
        # Global path file handling
        self.paths = read_json("config/paths.json")
        self.paths_host = self.paths["host"]
        # base file containing a valid list of hosts
        # self.config_path = self.paths_host["host"]
        self.config_tunnels_path = self.paths_host["sample_tunnels_config"]
        self.config_server_path = self.paths_host["sample_server_config"]
        self.tunnel_config = self.paths_host["sample_yovo-tunnel-template"]
        self.server_config = self.paths_host["sample_yovo-server-template"]
        self.config_port_low = self.paths_host["sample_config_port_low"]
        self.config_port_high = self.paths_host["sample_config_port_high"]
        self.config_port_too_high_for_voters = self.paths_host["sample_config_port_too_high"]
        if not os.path.isdir("output"):
            os.makedirs("output")

    ################################
    # init
    ################################
    def test_init_nominal(self):
        host_list = HostList(
            self.server_config, self.tunnel_config, config_server_path=self.config_server_path,
        )
        assert host_list.config_server_path == self.config_server_path
        assert host_list.template_tunnel_path == self.tunnel_config
        assert host_list.template_server_path == self.server_config

    def test_init_config_server_not_exists(self):
        with pytest.raises(IOError):
            HostList(
                self.server_config, self.tunnel_config, config_server_path="sample_not_exists",
            )

    def test_init_template_server_not_exists(self):
        with pytest.raises(IOError):
            HostList(
                "sample_not_exists", self.tunnel_config, config_server_path=self.config_server_path,
            )

    def test_init_template_tunnels_not_exists(self):
        with pytest.raises(IOError):
            HostList(
                self.server_config, "sample_not_exists", config_server_path=self.config_server_path,
            )

    def test_init_out_of_range_port_high(self):
        # Test if the starting port is usable
        with pytest.raises(ValueError):
            HostList(
                self.server_config, self.tunnel_config, config_server_path=self.config_port_high,
            )

    def test_init_out_of_range_port_low(self):
        # Test if the starting port is usable
        with pytest.raises(ValueError):
            HostList(
                self.server_config, self.tunnel_config, config_server_path=self.config_port_low,
            )

    ################################
    # load_tunnels_config
    ################################

    def test_load_tunnels_config_nominal(self):
        host_list = HostList(
            self.server_config, self.tunnel_config, config_server_path=self.config_server_path,
        )
        host_list.load_tunnels_config(self.config_tunnels_path)
        assert host_list.tunnels == {
            "1": "addr1.b32.i2p",
            "3": "addr3.b32.i2p",
            "4": "addr4.b32.i2p",
        }

    def test_load_tunnels_config_path_not_exists(self):
        host_list = HostList(
            self.server_config, self.tunnel_config, config_server_path=self.config_server_path,
        )
        with pytest.raises(IOError):
            host_list.load_tunnels_config("not_a_directory/tunnels.json")

    def test_load_tunnels_config_empty_id(self):
        host_list = HostList(
            self.server_config, self.tunnel_config, config_server_path=self.config_server_path,
        )
        with pytest.raises(ValueError):
            host_list.load_tunnels_config(self.paths_host["sample_tunnels_config_empty_id"])

    def test_load_tunnels_config_empty_address(self):
        host_list = HostList(
            self.server_config, self.tunnel_config, config_server_path=self.config_server_path,
        )
        with pytest.raises(ValueError):
            host_list.load_tunnels_config(self.paths_host["sample_tunnels_config_empty_address"])

    def test_load_tunnels_config_port_too_hight_for_ng_voters(self):
        host_list = HostList(
            self.server_config,
            self.tunnel_config,
            config_server_path=self.config_port_too_high_for_voters,
        )
        with pytest.raises(ValueError):
            host_list.load_tunnels_config(self.config_tunnels_path)

    ################################
    # generate_i2p_tunnels
    ################################

    def test_generate_i2p_tunnels_nominal(self):
        # Test if the written files are conform
        host_list = HostList(
            self.server_config, self.tunnel_config, config_server_path=self.config_server_path,
        )
        host_list.load_tunnels_config(self.config_tunnels_path)
        host_list.generate_i2p_tunnels("output/")
        assert filecmp.cmp(
            "output/yovo-tunnel-1.config", self.paths_host["sample_ref_yovo-tunnel-1"]
        )
        assert filecmp.cmp(
            "output/yovo-tunnel-3.config", self.paths_host["sample_ref_yovo-tunnel-3"]
        )
        assert filecmp.cmp(
            "output/yovo-tunnel-4.config", self.paths_host["sample_ref_yovo-tunnel-4"]
        )

    def test_generate_i2p_tunnels_output_not_exists(self):
        # Test if the written files are conform
        host_list = HostList(
            self.server_config, self.tunnel_config, config_server_path=self.config_server_path,
        )
        host_list.load_tunnels_config(self.config_tunnels_path)
        with pytest.raises(IOError):
            host_list.generate_i2p_tunnels("not_a_directory/")

    def test_generate_i2p_tunnels_no_tunnel(self):
        # Test if the written files are conform
        host_list = HostList(
            self.server_config, self.tunnel_config, config_server_path=self.config_server_path,
        )
        with pytest.raises(Exception):
            host_list.generate_i2p_tunnels("not_a_directory/")

    ################################
    # generate_i2p_server
    ################################

    def test_generate_i2p_server_nominal(self):
        # Test if the written files are conform
        host_list = HostList(
            self.server_config, self.tunnel_config, config_server_path=self.config_server_path,
        )
        host_list.generate_i2p_server("output/")
        assert filecmp.cmp(
            "output/yovo-server-2.config", self.paths_host["sample_ref_yovo-server-2"]
        )

    def test_generate_i2p_server_output_not_exists(self):
        host_list = HostList(
            self.server_config, self.tunnel_config, config_server_path=self.config_server_path,
        )
        with pytest.raises(IOError):
            host_list.generate_i2p_server("not_a_directory/")

    ################################
    # generate_mpspdz_config
    ################################

    def test_generate_mpspdz_config_nominal(self):
        # Test if the config written is conform
        host_list = HostList(
            self.server_config, self.tunnel_config, config_server_path=self.config_server_path,
        )
        host_list.load_tunnels_config(self.config_tunnels_path)
        host_list.generate_mpspdz_config("output/HOST")
        assert filecmp.cmp("output/HOST", self.paths_host["sample_ref_HOST"])

    def test_generate_mpspdz_config_no_tunnel(self):
        host_list = HostList(
            self.server_config, self.tunnel_config, config_server_path=self.config_server_path,
        )
        with pytest.raises(Exception):
            host_list.generate_mpspdz_config("output/HOST")

    def test_generate_mpspdz_config_output_not_exists(self):
        host_list = HostList(
            self.server_config, self.tunnel_config, config_server_path=self.config_server_path,
        )
        host_list.load_tunnels_config(self.config_tunnels_path)
        with pytest.raises(IOError):
            host_list.generate_mpspdz_config("not_a_directory/HOST")


if __name__ == "__main__":
    print("This file should be used through py.test")
    sys.exit(1)
