"""
All tests for candidate.py
"""
# pylint: disable=missing-function-docstring,attribute-defined-outside-init,no-self-use

import sys
import pytest

from yovo.candidate import CandidateList
from yovo.utils import read_json


class TestCandidateList:
    """
    Test suite for CandidateList object
    """

    def setup_class(self):
        paths = read_json("config/paths.json")
        paths_candidates = paths["candidate"]
        self.path1 = paths_candidates["sample_candidate_1"]
        self.path2 = paths_candidates["sample_candidate_2"]

    ################################
    # init()
    ################################
    def test_init_nominal(self):
        # Nominal usecase
        existing_cl = CandidateList(self.path1)
        assert existing_cl.candidates_nb != 0

    ## Commented till error handling / module is ok
    def test_init_empty(self):
        # Nominal usecase with empty path
        empty_cl = CandidateList()
        assert empty_cl.candidates_nb == 0

    def test_init_file_not_found(self):
        # Error usecase path not existing
        with pytest.raises(IOError):
            empty_cl = CandidateList("highly_unlikely_file_name_1549684894651444")
            assert empty_cl.candidates_nb == 0

    ################################
    # add_candidate()
    ################################

    def test_add_candidate_nominal(self):
        candid_list = CandidateList()
        ctrl_sample = ["Didier"]
        candid_list.add_candidate("Didier")
        assert candid_list.candidates == ctrl_sample

    def test_add_candidate_space_latin(self):
        # New candidate format should allow spaces and non ASCII chars
        candid_list = CandidateList()
        ctrl_sample = ["Jean-René Bourgeois de La Motte"]
        candid_list.add_candidate("Jean-René Bourgeois de La Motte")
        assert candid_list.candidates == ctrl_sample

    def test_candidate_utf8_add(self):
        # New candidate format should respect UTF-8 chars
        candid_list = CandidateList()
        ctrl_sample = ["张伟"]
        candid_list.add_candidate("张伟")
        assert candid_list.candidates == ctrl_sample

    # ################################
    # # path
    # ################################
    def test_path_change_nominal(self):
        # on path change, candidates should load
        # test depends on init(path)
        candid_list = CandidateList(self.path1)
        assert candid_list.candidates_nb == 3
        candid_list.path = self.path2
        assert candid_list.candidates_nb == 4

    def test_path_get(self):
        candid_list = CandidateList(self.path1)
        assert candid_list.path == self.path1

    def test_path_change_empty(self):
        candid_list = CandidateList(self.path1)
        assert candid_list.candidates_nb == 3
        candid_list.path = ""
        assert candid_list.candidates_nb == 0

    def test_path_change_file_not_found(self):
        candid_list = CandidateList(self.path1)
        assert candid_list.candidates_nb == 3
        with pytest.raises(IOError):
            candid_list.path = "highly_unlikely_file_name_1549684894651444"

    ################################
    # _clear()
    ################################
    def test_clear(self):
        # CandidateList shall be empty after clear
        # Test rely on loading on init
        candid_list = CandidateList(self.path1)
        candid_list._clear()  # pylint: disable=protected-access
        assert candid_list.candidates_nb == 0

    # ################################
    # # load_candidate_file()
    # ################################
    def test_load_candidate_file(self):
        candid_list = CandidateList()
        candid_list.load_candidate_file(self.path1)
        name_set = ["Patate", "Jean-René de La Motte", "张伟"]
        assert candid_list.candidates == name_set

    def test_load_candidate_file_2_times(self):
        candid_list = CandidateList()
        candid_list.load_candidate_file(self.path1)
        assert candid_list.candidates_nb == 3
        candid_list.load_candidate_file(self.path2)
        assert candid_list.candidates_nb == 4

    def test_load_candidate_no_path(self):
        candid_list = CandidateList()
        with pytest.raises(IOError):
            candid_list.load_candidate_file("")

    def test_load_candidate_file_invalid_path(self):
        candid_list = CandidateList()
        with pytest.raises(IOError):
            candid_list.load_candidate_file("highly_unlikely_file_name_1549684894651444")


if __name__ == "__main__":
    print("This file should be used through pytest")
    sys.exit(1)
