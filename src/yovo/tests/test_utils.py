"""
All tests for utils.py
"""
# pylint: disable=missing-function-docstring,attribute-defined-outside-init,no-self-use

import sys

import pytest

from yovo.utils import validate_paths, read_json, write_json


class TestUtils:
    """
    Test suite for utils.py functions
    """

    def setup_class(self):
        self.paths = read_json("config/paths.json")
        self.bad_paths = read_json(self.paths["util"]["sample_not_a_path"])
        self.bad_json = read_json(self.paths["util"]["sample_not_a_json"])

    ################################
    # validate_paths
    ################################

    def test_validate_paths_not_a_file(self):
        """
        Assert that test_path detect files that are not existant
        """
        with pytest.raises(FileNotFoundError):
            validate_paths(self.paths["util"]["sample_not_a_path"])

    def test_validate_paths_not_a_json(self):
        """
        Assert that test_path detect invalid json file
        """
        with pytest.raises(ValueError):
            validate_paths(self.paths["util"]["sample_not_a_json"])

    def test_validate_paths_nominal(self):
        """
        Test existing paths.json correctness ()
        """

        assert validate_paths("config/paths.json") is None

    ################################
    # read_json
    ################################

    def test_read_json_nominal(self):
        """
        Test nominal use case of read_json
        """
        assert read_json(self.paths["candidate"]["sample_candidate_1"]) == [
            "Patate",
            "Jean-René de La Motte",
            "张伟",
        ]

    def test_read_json_no_file(self):
        """
        Test nominal use case of read_json
        """
        with pytest.raises(OSError):
            read_json(self.bad_paths["candidate"]["sample_definetly_not_a_path"])

    def test_read_json_no_json(self):
        """
        Nominal use case of read_json
        """
        with pytest.raises(ValueError):
            read_json(self.bad_json["candidate"]["invalid_json"])

    ################################
    # write_json
    ################################

    def test_write_json_nominal(self):
        dummy = self.paths["util"]["sample_dummy"]
        write_json(["Patate", "Jean-René de La Motte", "张伟"], dummy)
        assert read_json(dummy) == ["Patate", "Jean-René de La Motte", "张伟"]

    def test_write_json_empty(self):
        dummy = self.paths["util"]["sample_dummy"]
        write_json("", dummy)
        assert read_json(dummy) == ""

    def test_write_json_invalid_path(self):
        with pytest.raises(IOError):
            write_json(["Patate", "Jean-René de La Motte", "张伟"], "not_a_directory/dummy.json")


if __name__ == "__main__":
    print("This file should be used through pytest")
    sys.exit(1)
