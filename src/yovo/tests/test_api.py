"""
Contains all tests for API defined in main.py
"""
# pylint: disable=missing-function-docstring,attribute-defined-outside-init,no-self-use

import sys
import os

from yovo.utils import read_json, write_json


class TestApi:
    """
    Test suit for Flask API
    """

    def setup_class(self):
        self.paths = read_json("config/paths.json")
        self.paths_candidates = self.paths["candidate"]

    #############################
    #       GET ENDPOINTS       #
    #############################

    #############################
    #       /vote/results       #
    #############################
    # {
    #   "cand1": 0,
    #   "cand2": 4,
    #   "cand3: 2
    # }

    def test_vote_result_nominal(self, client):
        write_json(
            read_json(self.paths["result"]["sample_result_ref_1"]), self.paths["output"]["result"]
        )
        res = client.get("/vote/results")
        os.remove(self.paths["output"]["result"])
        assert res.status_code == 200
        assert res.json == {"Robert": 1, "Jean": 1, "René": 2}

    def test_vote_result_empty(self, client):
        res = client.get("/vote/results")
        assert res.status_code == 200
        assert res.json == {}

    #############################
    #       others              #
    #############################

    def test_favicon_nominal(self, client):
        res = client.get("/favicon.ico")
        assert res.status_code == 200

    def test_index_nominal(self, client):
        res = client.get("/index")
        assert res.status_code == 200

    def test_about_nominal(self, client):
        res = client.get("/about")
        assert res.status_code == 200

    def test_how_to_nominal(self, client):
        res = client.get("/how-to")
        assert res.status_code == 200

    #############################
    #       POST ENDPOINTS      #
    #############################

    #############################
    #     /candidates/change    #
    #############################
    # [
    #   "candid1",
    #   "candid2",
    #   "candid3",
    # ]

    def test_candidate_change_nominal(self, client):
        res = client.post(
            "/candidate/change", json=read_json(self.paths_candidates["sample_candidate_1"])
        )
        assert res.status_code == 200

    #############################
    #         /i2p/setup        #
    #############################
    # {
    #     "party_id":2,
    #     "starting_port":6000,
    # }

    def test_i2p_setup_ordered_calls(self, client):
        res = client.post(
            "/candidate/change", json=read_json(self.paths_candidates["sample_candidate_1"])
        )
        res = client.post("/i2p/setup", json=read_json(self.paths["host"]["sample_server_config"]))
        assert res.status_code == 200

    def test_i2p_setup_unordered_calls(self, client):
        res = client.post("/i2p/setup", json=read_json(self.paths["host"]["sample_server_config"]))
        assert res.status_code == 500
        assert "Please provide candidates before" in str(res.data)

    def test_i2p_setup_wrong_data(self, client):
        res = client.post(
            "/candidate/change", json=read_json(self.paths_candidates["sample_candidate_1"])
        )
        res = client.post(
            "/i2p/setup", json=read_json(self.paths["host"]["sample_config_port_high"])
        )
        assert res.status_code == 500
        assert "Starting port number in a bad range" in str(res.data)

    #############################
    #       /i2p/tunnels        #
    #############################
    # {
    #   "1": "addr1.b32.i2p",
    #   "3": "addr3.b32.i2p",
    #   "4": "addr4.b32.i2p",
    # }

    def test_i2p_tunnels_nominal_ordered(self, client):
        res = client.post(
            "/candidate/change", json=read_json(self.paths_candidates["sample_candidate_1"])
        )
        res = client.post("/i2p/setup", json=read_json(self.paths["host"]["sample_server_config"]))
        res = client.post(
            "i2p/tunnels", json=read_json(self.paths["host"]["sample_tunnels_config"])
        )
        assert res.status_code == 200

    def test_i2p_tunnels_nominal_unordered(self, client):
        res = client.post(
            "i2p/tunnels", json=read_json(self.paths["host"]["sample_tunnels_config"])
        )
        assert res.status_code == 500
        assert "Please provide I2P server config before" in str(res.data)

    #############################
    #         /vote/start       #
    #############################
    # {
    #   "date": "YYYY-MM-DD HH:MM"
    # }

    def test_vote_start_nominal_with_date(self, client, mocker):
        res = client.post(
            "/candidate/change", json=read_json(self.paths_candidates["sample_candidate_1"])
        )
        res = client.post("/i2p/setup", json=read_json(self.paths["host"]["sample_server_config"]))
        res = client.post(
            "i2p/tunnels", json=read_json(self.paths["host"]["sample_tunnels_config"])
        )

        mock_thread = mocker.patch("threading.Thread.start")
        mock_timer = mocker.patch("threading.Timer.start")

        res = client.post("/vote/start", json=read_json(self.paths["api"]["sample_i2p_start_date"]))
        assert res.status_code == 200

        mock_timer.assert_called_once()
        assert not mock_thread.called
        mocker.stopall()

    def test_vote_start_nominal_without_date(self, client, mocker):
        res = client.post(
            "/candidate/change", json=read_json(self.paths_candidates["sample_candidate_1"])
        )
        res = client.post("/i2p/setup", json=read_json(self.paths["host"]["sample_server_config"]))
        res = client.post(
            "i2p/tunnels", json=read_json(self.paths["host"]["sample_tunnels_config"])
        )
        mock_thread = mocker.patch("threading.Thread.start")
        mock_timer = mocker.patch("threading.Timer.start")

        res = client.post(
            "/vote/start", json=read_json(self.paths["api"]["sample_i2p_start_no_date"])
        )
        assert res.status_code == 200

        mock_thread.assert_called_once()
        assert not mock_timer.called
        mocker.stopall()

    def test_vote_start_delete_old_result(self, client, mocker):
        res = client.post(
            "/candidate/change", json=read_json(self.paths_candidates["sample_candidate_1"])
        )
        res = client.post("/i2p/setup", json=read_json(self.paths["host"]["sample_server_config"]))
        res = client.post(
            "i2p/tunnels", json=read_json(self.paths["host"]["sample_tunnels_config"])
        )

        write_json(
            read_json(self.paths["result"]["sample_result_ref_1"]), self.paths["output"]["result"]
        )

        mock_thread = mocker.patch("threading.Thread.start")
        mock_timer = mocker.patch("threading.Timer.start")
        mock_remove = mocker.patch("os.remove")

        res = client.post(
            "/vote/start", json=read_json(self.paths["api"]["sample_i2p_start_no_date"])
        )
        assert res.status_code == 200

        mock_thread.assert_called_once()
        mock_remove.assert_called_once()
        assert not mock_timer.called
        mocker.stopall()

        os.remove(self.paths["output"]["result"])

    def test_vote_start_unordered(self, client):
        res = client.post(
            "/vote/start", json=read_json(self.paths["api"]["sample_i2p_start_no_date"])
        )
        assert res.status_code == 500
        assert "Please provide I2P tunnels config before" in str(res.data)


if __name__ == "__main__":
    print("This file should be used through pytest")
    sys.exit(1)
