"""
This module implement a CLI to run a vote with YOVO
"""
# pylint: disable=wrong-import-position

import os
import sys
import time

sys.path.insert(0, os.path.abspath(".."))

from yovo.interface import Interface
from yovo.utils import get_config, setup_logging, read_json, AUTO_RESTART, handle_file_open_error

logger = setup_logging()
interface = Interface(config=get_config())


def ask(question):
    """
    Ask a yes/no question to the user

    Parameters:
        question (str): The question to ask
    Returns:
        True/False (bool): La réponse de l'utilisateur
    """

    push = input(question + " (y/n) ")
    return push in ("Y", "y")


def clear():
    """ Clear terminal """

    os.system("clear")


def print_title(title):
    """ Show banner for the CLI. """

    print(
        "\
        ██╗   ██╗ ██████╗ ██╗   ██╗ ██████╗\n\
        ╚██╗ ██╔╝██╔═══██╗██║   ██║██╔═══██╗\n\
        ╚████╔╝ ██║   ██║██║   ██║██║   ██║\n\
          ╚██╔╝  ██║   ██║╚██╗ ██╔╝██║   ██║\n\
           ██║   ╚██████╔╝ ╚████╔╝ ╚██████╔╝\n\
           ╚═╝    ╚═════╝   ╚═══╝   ╚═════╝ \n\
        # YOU ONLY VOTE ONCE\n"
    )
    print("=============================================================================")
    print(title)
    print("=============================================================================")


def print_candidates(json):
    """ Print candidates from JSON object. """

    print("Candidate list :")
    if not json:
        print("<empty>")
    else:
        for i, item in enumerate(json):
            print(i, "-", item)


def print_results(json):
    """ Print results from JSON object. """

    print("=============================================================================")
    print("Results :")
    if not json:
        print("<empty>")
    else:
        for key, value in json.items():
            print(key, "-", value)


def print_tunnels(json):
    """ Print tunnels from JSON object. """

    print("Tunnels list :")
    if not json:
        print("<empty>")
    else:
        for key, value in json.items():
            print(key, "-", value)


def get_int(question):
    """
    Get INT from user input
    """

    while True:
        try:
            print(question)
            user_input = int(input("> "))
        except ValueError:
            print("Please provide an integer.")
        else:
            return user_input


def get_json_from_file():
    """ Retrieve a JSON from path provided by user. """

    while True:
        print("Enter relative or absolute path to file, enter [c] to cancel")
        path_to_file = input("> ")
        if path_to_file == "c":
            return None
        try:
            return read_json(path_to_file)
        except IOError as exception:
            handle_file_open_error(exception, path_to_file)


def get_candidates_manual():
    """ Get candidates one-by-one form user input. """

    candidates = []
    print("Enter candidates names separated by [ENTER] and stop with blank entry")
    while True:
        name = input("> ")
        if name:
            candidates.append(name)
        else:
            break
    return candidates


def get_candidate_list():
    """
    Ask to provide a candidate list manually or from file
    """

    got_candidates = False
    while not got_candidates:
        print_title("1/4 : Candidate (S)election")
        print("1 - Provide candidates from file")
        print("2 - Provide candidates manually")

        choice = get_int("enter [0] to cancel")
        if choice == 0:  # pylint: disable=no-else-break
            break
        elif choice == 1:
            candidates = get_json_from_file()
            print_candidates(candidates)
            if ask("Do you agree with candidates retrieved from file ?"):
                interface.setup_candidates(candidates)
                got_candidates = True
        elif choice == 2:
            while True:
                candidates = get_candidates_manual()
                print_candidates(candidates)
                if ask("Do you agree with candidates retrieved from file ?"):
                    interface.setup_candidates(candidates)
                    got_candidates = True
                    break
        else:
            logger.error("Choice is not 1 or 2")
        time.sleep(2)
        clear()
    return candidates


def get_i2p_server_config():
    """
    Ask to provide I2P server config
    """

    print_title("2/4 : I2P Server configuration")
    while True:
        port = get_int("Enter starting port number [1024, 65534] :")
        if not 1024 <= port < 65534:
            logger.error("Port provided is not in required range")
        else:
            break
    party_id = get_int("Enter party ID :")
    interface.setup_i2p_server({"party_id": party_id, "starting_port": port})


def get_tunnels_manual():
    """ Get tunnels one by one from input. """

    tunnels = {}
    print("Enter tunnels (<party_id>:<addr>) separated by [ENTER] and stop with blank entry")
    while True:
        entry = input("> ")
        try:
            tunnel = entry.split(":")
            if tunnel[1] and int(tunnel[0]):
                tunnels[tunnel[0]] = tunnel[1]
            elif not entry:
                break
            else:
                logger.error("Tunnel address is empty")
        except ValueError:
            logger.error("Party ID is not a number")
    return tunnels


def get_i2p_tunnels_config():
    """
    Ask to provide I2P tunnels config manually or from file
    """

    got_tunnels = False
    while not got_tunnels:
        print_title("3/4 : I2P Tunnels configuration")
        print("1 - Provide tunnels from file")
        print("2 - Provide tunnels manually")

        choice = get_int("enter [0] to cancel")
        if choice == 0:  # pylint: disable=no-else-break
            break
        elif choice == 1:
            tunnels = get_json_from_file()
            print_tunnels(tunnels)
            if ask("Do you agree with tunnels retrieved from file ?"):
                interface.setup_vote(tunnels)
                got_tunnels = True
            else:
                logger.error("Check your file or path to it if you don't agree")
        elif choice == 2:
            while True:
                tunnels = get_tunnels_manual()
                print_tunnels(tunnels)
                if ask("Do you agree with tunnels retrieved from file ?"):
                    interface.setup_vote(tunnels)
                    got_tunnels = True
                    break
        else:
            logger.error("Choice is not 1 or 2")
        time.sleep(2)
        clear()
    return got_tunnels


def start_vote(candidates):
    """
    Start vote after providing chosen candidate
    """

    print_title("4/4 : Vote")
    print_candidates(candidates)
    while True:
        vote = get_int("Choose your candidate :")
        if not 0 <= vote < len(candidates):
            logger.error("Candidate ID out of range")
        else:
            break

    thread = None
    while True:
        print("1 - Start vote now")
        print("2 - Start vote later")

        choice = get_int("enter [0] to cancel")
        if choice == 0:  # pylint: disable=no-else-break
            break
        elif choice == 1:
            thread = interface.prepare_vote({"vote": vote, "date": ""})
        elif choice == 2:
            print("Enter date and time to start (YYYY-mm-dd HH:MM) :")
            date = input("> ")
            thread = interface.prepare_vote({"vote": vote, "date": date})
        else:
            logger.error("Choice is not 1 or 2")
        time.sleep(2)
        clear()
    return thread


def main():
    """ Main CLI thread . """

    if os.name == "nt":
        logger.error("You can't run this program on Windows, need I2P to be installed")
        sys.exit(0)
    clear()
    candidates = get_candidate_list()
    if not candidates:
        sys.exit(0)
    clear()
    get_i2p_server_config()

    if AUTO_RESTART:
        os.system("sudo service i2pd restart")
    else:
        print("Please restart i2p before continue")
        time.sleep(5)

    clear()
    if not get_i2p_tunnels_config():
        sys.exit(0)

    if AUTO_RESTART:
        os.system("sudo service i2pd restart")
    else:
        print("Please restart i2p before continue")
        time.sleep(5)

    thread = start_vote(candidates)
    if thread:
        print("Waiting for result ...")
        thread.join()
        print_results(interface.read_results())


if __name__ == "__main__":
    main()
