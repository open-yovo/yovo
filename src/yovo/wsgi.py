"""
Short code to provide Flask app to Gunicorn
"""
# pylint: disable=wrong-import-position

import os
import sys

sys.path.insert(0, os.path.abspath(".."))

from yovo.main import create_app

app = create_app()
