"""
This module describe a managment interface to use the vote object
"""

import threading
import os
import json
import logging

from datetime import datetime

from yovo.vote import Vote
from yovo.utils import get_config, write_json, StateException

logger = logging.getLogger("yovo")


class Interface:
    """
    An interface between flask API and vote object
    """

    def __init__(self, config=get_config()):  # pylint: disable=dangerous-default-value
        logger.info("[INTERFACE] Instantiating interface object")
        self.vote = None
        self.config = config
        self.state = 0

    def setup_i2p_server(self, data):
        """
        Instantiate voting object and generate hidden service configuration for i2p

        Parameters:
            data : :obj:`json`
            should contain party_id and starting port, formatted like that :
            {
            "party_id":2,
            "starting_port":6000
            }
        """

        logger.info("[INTERFACE] Initializing I2P with starting port and vote with party ID")
        write_json(data, self.config["host-server"])
        self.vote = Vote(config=self.config)
        if self.state >= 1:
            self.vote.party_id = int(data["party_id"])
            self.vote.generate_i2p_server()
            self.state = 2
        else:
            logger.error(
                f"[INTERFACE] Couldn't setup I2P server, state is at {self.state} needed >= 1"
            )
            raise StateException("Please provide candidates before")

    def setup_vote(self, data):
        """
        Load list of tunnels for i2p, check the configuration and generate the config files for i2p

        Parameters:
            data : :obj:`json`
                list of tunnels with id, formatted like that :
                {
                "1": "addr1.b32.i2p",
                "3": "addr3.b32.i2p",
                "4": "addr4.b32.i2p"
                }
        """

        logger.info("[INTERFACE] Setting up I2P tunnels config")
        if self.state >= 2:
            logger.debug("[INTERFACE] Writing config of I2P tunnels to file")
            write_json(data, self.config["host-tunnels"])
            logger.debug("[INTERFACE] Generating I2P tunnels config")
            self.vote.generate_i2p_tunnels()
            self.vote.check_configuration()
            self.state = 3
        else:
            logger.error(
                f"[INTERFACE] Couldn't setup I2P tunnels, state is at {self.state} needed >= 2"
            )
            raise StateException("Please provide I2P server config before")

    def setup_candidates(self, candid_list):
        """
        Define vote candidates

        Parameters:
            candid_list: :obj:`list`
                Candidates list
        """

        logger.info("[INTERFACE] Writing candidate list to file")
        write_json(candid_list, self.config["candidates"])
        self.state = 1

    def _write_vote(self, vote):
        """
        Write vote to config file

        Parameters:
            vote: :obj:`int`
                Candidate ID of voter's choice
        """

        logger.info("[INTERFACE] Writing vote to file")
        write_json(vote, self.config["vote"])

    def prepare_vote(self, data):
        """
        Prepare vote by calling pre-vote functions and shedule vote start at desired date

        Parameters:
            data: :obj:`dict`
                Data sent from API
        """

        self._write_vote(data)
        logger.info("[INTERFACE] Preparing vote before running")
        if self.state >= 3:
            logger.debug("[INTERFACE] Removing old vote result if it exists")
            if os.path.isfile(self.config["result"]):
                os.remove(self.config["result"])
            self.vote.check_ballot_max_size()
            self.vote.compile_program()
            self.vote.encode_vote()

            run_at = data["date"]
            if run_at.replace(" ", ""):  # pylint: disable=no-else-return
                logger.info(f"[INTERFACE] Preparing thread for delayed vote run at {run_at}")
                date = datetime.strptime(run_at, "%Y-%m-%d %H:%M")
                now = datetime.now()
                delay = (date - now).total_seconds()
                thread = threading.Timer(delay, self._run_vote)
                thread.deamon = True
                thread.start()
                return thread
            else:
                logger.info("[INTERFACE] Preparing thread for instant run")
                thread = threading.Thread(target=self._run_vote)
                thread.deamon = True
                thread.start()
                return thread
        else:
            logger.error(f"[INTERFACE] Couldn't start vote, state is at {self.state} needed >= 3")
            raise StateException("Please provide I2P tunnels config before")

    def _run_vote(self):
        """
        Run vote and write results to output file
        """

        logger.info("[INTERFACE] Start voting process")
        try:
            self.vote.run_program()
            self.vote.read_ballot()
            self.vote.decode_ballot()
            self.vote.map_result()
            self.state = 4
        except Exception as exception:  # pylint: disable=broad-except
            logger.error(f"[INTERFACE] Error happended while running vote : {str(exception)}")

    def read_results(self):
        """
        Read results from output file and return them

        Returns:
            :obj:`dict`: Results as {<candidate>: <number_of_vote>, ...}
        """

        logger.info("[INTERFACE] Reading vote result from file")
        if os.path.isfile(self.config["result"]):
            logger.debug("[INTERFACE] Found the vote result file")
            with open(self.config["result"], "r", encoding="utf-8") as results:
                return json.load(results)
        else:
            logger.debug("[INTERFACE] No file found with vote result, returning empty value")
            return {}
