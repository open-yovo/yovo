"""
Manage hosts used in the vote system
"""

import json
import logging

from jinja2 import Template

from yovo.utils import handle_file_open_error, handle_file_write_error

logger = logging.getLogger("yovo")


class HostList:  # pylint: disable=too-many-instance-attributes
    """
    Define host list used by i2p

    Attributes:
        tunnels_nb: :obj: `int`
            Number of tunnels required
        tunnel: :obj: `list`
            List of registered tunnels
        template_server_path : :obj: `str`
            path of the template for the server config
        template_tunnel_path : :obj: `str`
            path of the template for the tunnel config
    """

    # pylint: disable=bad-continuation
    def __init__(
        self, template_server_path, template_tunnel_path, config_server_path="",
    ):
        logger.info("[HOST] Instantiating host object")
        self._config_server_path = config_server_path
        self._template_server_path = template_server_path
        self._template_tunnel_path = template_tunnel_path

        self.server_template = None
        self.tunnel_template = None
        self.starting_port = None
        self.party_id = None
        self.party_len = None
        self.tunnels = None
        self.load_server_config(self._config_server_path)
        self._load_server_template()
        self._load_tunnel_template()

    @property
    def config_server_path(self):
        """ Property to save path to server configuration. """

        return self._config_server_path

    @property
    def template_server_path(self):
        """ Property to save path to server configuration template. """

        return self._template_server_path

    @property
    def template_tunnel_path(self):
        """ Property to save path to tunnel configuration template. """

        return self._template_tunnel_path

    def load_server_config(self, path):
        """
        Load server config from a specified file's path,
        server config contains the party id and the starting port

        Parameters:
            path: :obj:`str`
                Path to config file
        """

        logger.info("[HOST] Loading I2P server config")
        try:
            logger.debug("[HOST] Getting I2P server config from file")
            with open(path, "r", encoding="utf-8") as file_handle:
                config = json.loads(file_handle.read())
        except IOError as exception:
            handle_file_open_error(exception, path)
        temp_party_id = int(config["party_id"])
        temp_starting_port = int(config["starting_port"])
        logger.debug("[HOST] Checking port is in required interval [1024, 65534]")
        if temp_starting_port not in range(1024, 65535):
            raise ValueError("Starting port number in a bad range")
        logger.debug("[HOST] Setting I2P server config on object attributes")
        self.party_id = temp_party_id
        self.starting_port = temp_starting_port

    def load_tunnels_config(self, path):
        """
        Load tunnels config from a specified file's path
        tunnels config contains a list of id : i2p address

        Parameters:
            path: :obj:`str`
                Path to config file
        """

        logger.debug("[HOST] Loading I2P tunnels config")
        try:
            logger.debug("[HOST] Getting I2P tunnels config from file")
            with open(path, "r", encoding="utf-8") as file_handle:
                config = json.loads(file_handle.read())
        except IOError as exception:
            handle_file_open_error(exception, path)
        temp_tunnels = config
        logger.debug("[HOST] Checking tunnels config's port fit in required interval [1024, 65535]")
        for party_id, addr in temp_tunnels.items():
            if party_id == "" or addr == "":
                raise ValueError("Empty value in the tunnels list")
        if self.starting_port + int(max(temp_tunnels.keys())) > 65535:
            raise ValueError("Starting port is too high for the number of voters")
        logger.debug("[HOST] Setting I2P tunnels config to object attribute")
        self.tunnels = temp_tunnels
        return len(self.tunnels)

    def _load_server_template(self):
        """
        Load server template from a specified file's path
        """

        logger.info("[HOST] Loading I2P server template config")
        try:
            with open(self._template_server_path, "r", encoding="utf-8") as file_handle:
                self.server_template = Template(file_handle.read())
        except IOError as exception:
            handle_file_open_error(exception, self._template_server_path)

    def _load_tunnel_template(self):
        """
        Load tunell template from a specified file's path
        """

        logger.info("[HOST] Loading I2P tunnels template config")
        try:
            with open(self._template_tunnel_path, "r", encoding="utf-8") as file_handle:
                self.tunnel_template = Template(file_handle.read())
        except IOError as exception:
            handle_file_open_error(exception, self._template_tunnel_path)

    def generate_i2p_tunnels(self, path):
        """
        Write multiple files for i2pd config

        Parameters:
            path: :obj: `str`
                Path to the tunnels.d folder of i2pd
        """

        # Create Tunnels config
        logger.info("[HOST] Generating I2P tunnels configuration")
        if not self.tunnels:
            raise Exception("Tunnels configuration need to be provided before")
        port = self.starting_port
        for party_id, addr in self.tunnels.items():
            tunnel = f"{path}/yovo-tunnel-{party_id}.config"
            logger.debug(f'[HOST] Generating tunnel config for ID {party_id} with address "{addr}"')
            try:
                with open(tunnel, "w", encoding="utf-8") as file_handle:
                    file_handle.write(
                        self.tunnel_template.render(
                            title=f"yovo-tunnel-{party_id}", port=port + int(party_id), dest=addr
                        )
                    )
            except IOError:
                handle_file_write_error(path)

    def generate_i2p_server(self, path):
        """
        Write multiple files for i2pd config

        Parameters:
            path: :obj: `str`
                Path to the tunnels.d folder of i2pd
        """

        logger.info("[HOST] Generating I2P server configuration")
        port = self.starting_port
        # Create Hidden Service config
        tunnel = f"{path}yovo-server-{self.party_id}.config"
        try:
            with open(tunnel, "w", encoding="utf-8") as file_handle:
                file_handle.write(
                    self.server_template.render(title="yovo-server", port=port + self.party_id)
                )
        except IOError:
            handle_file_write_error(path)

    def generate_mpspdz_config(self, path):
        """
        Write the host file for MP-SPDZ

        Parameters:
            path: :obj: `str`
                Path to the file to be written
        """

        logger.info("[HOST] Generating host list for MP-SPDZ")
        if not self.tunnels:
            raise Exception("Tunnels configuration need to be provided before")
        mpsdz_host_list = ""
        port = self.starting_port
        for _ in range(len(self.tunnels) + 1):
            mpsdz_host_list += f"127.0.0.1:{port}\n"
            port += 1
        logger.debug("[HOST] Writing MP-SPDZ host list to file")
        try:
            with open(path, "w", encoding="utf-8") as file_handle:
                file_handle.write(mpsdz_host_list)
        except IOError:
            handle_file_write_error(path)
