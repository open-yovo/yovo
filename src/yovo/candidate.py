"""
Define candidate managment
"""

import json
import logging

from yovo.utils import handle_file_open_error

logger = logging.getLogger("yovo")


class CandidateList:
    """
    Define candidate list used in the voting process

    Attributes:
        candidates_nb: :obj:`int`
            Number of candidates required
        candidates: :obj:`list`
            List of registered candidates
        path: :obj:`str`
            Path to the candidate file
    """

    def __init__(self, path=""):
        logger.info("[CANDIDATE] Instantiating candidate list object")
        self._candidates = []
        self.path = path

    @property
    def candidates_nb(self):
        """
        Get current candidates number

        Returns:
            :obj:`int`: Number of candidates registered
        """

        return len(self.candidates)

    @property
    def candidates(self):
        """ Define candidate array as property. """

        return self._candidates

    @candidates.setter
    def candidates(self, candidate):
        """ Define candidate array setter. """

        logger.debug(f"[CANDIDATE] Setting candidate list to {candidate}")
        self._candidates = candidate

    @property
    def path(self):
        """ Define candidate array path as property. """

        return self._path

    @path.setter
    def path(self, newpath):
        """ Define candidate array path setter. """

        logger.info(f"[CANDIDATE] Setting candidate list path to {newpath}")
        self._path = newpath
        if self._path:
            self.load_candidate_file(self._path)
        else:
            self.candidates = []

    def add_candidate(self, name):
        """
        Add candidate in a file

        Parameters:
            name: :obj:`str`
                name of the new candidate
        """

        logger.debug(f'[CANDIDATE] Adding candidate "{name}" to list')
        self.candidates.append(name)

    def load_candidate_file(self, path):
        """
        Load candidates from a file

        Parameters:
            path: :obj:`str`
                Path to candidate file
        """

        logger.info(f'[CANDIDATE] Loading candidates from file "{path}"')
        self._clear()
        try:
            logger.debug("[CANDIDATE] Reading candidates from file")
            with open(path, encoding="utf-8") as file_candidate:
                name_list = json.load(file_candidate)
                self.candidates = []
                for name in name_list:
                    self.add_candidate(name)
        except IOError as exception:
            handle_file_open_error(exception, path)  # fix no module yovo found

    def _clear(self):
        logger.info("[CANDIDATE] Clearing candidate list")
        del self.candidates[:]
