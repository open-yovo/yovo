import { Popup } from './utils.js'


/*
  method: POST or GET
  endpoint: the API endpoint to contact (needs to start with a slash /)
  data: the brute data (will be JSON.stringify)
  success/errorCallback: used based on the status' code returned by the API
*/
export function callApi(method, endpoint, data = '', successCallback, errorCallback) {
  let host = '127.0.0.1'
  let port = '5000'
  let url = 'http://'+ host + ':' + port + endpoint

  let req = new XMLHttpRequest()
  req.addEventListener('readystatechange', (e) => {requestHandler(req, successCallback, errorCallback)})

  console.warn(`Requesting endpoint ${url} with method ${method} with the following data:`)
  console.warn(data)

  req.open(method, url)

  if(method == 'POST' && data) {
    req.setRequestHeader('Content-Type', 'application/json')
    req.send(JSON.stringify(data))
  } else {
    req.send()
  }
}


function requestHandler(req, successCallback, errorCallback) {
  if(req.readyState === 4) {
    console.warn(`API responded with status ${req.status}`)
    if(req.status === 200) {
      successCallback(req.response)
    } else {
      console.log(req)
      /*
        note:
        the order of the following parameters helps using custom callbacks
        ie: using only myErrorCallback(status)
            or myErrorCallback(status, resp) works!
        so when adding new needed parameters, add them at the end of the
        following callback:
      */
      errorCallback(req.status, req.response, req.responseURL)
    }
  }
}
