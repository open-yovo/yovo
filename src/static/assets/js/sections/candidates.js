import { Popup, addRowToTable } from '../utils.js'
import { goToSection } from '../nav.js'
import { callApi } from '../api.js'
import { addCandidateToSelect } from './casting.js'

export const CAND = {
  start() {
    let section = document.getElementById('candidates')
    this.ui = {
      'input': document.getElementById('candidate'),
      'table': section.getElementsByTagName('table')[0],
      'addButton': document.getElementById('addcandidate'),
      'submitButton': document.getElementById('subcandidates')
    }
    this.emptyTable = true
    this.setListeners()
  }, // start()

  setListeners() {
    this.ui.input.addEventListener('keypress', addCandidate)
    this.ui.addButton.addEventListener('click', addCandidate)
    this.ui.submitButton.addEventListener('click', validateCandidates)
  },

  getCurrentCandidates() {
    let res = []
    for(let items of this.ui.table.getElementsByClassName('candidate')) {
      res.push(items.innerHTML)
    }
    return res
  }
}


function addCandidate(e) {
  // checking if the input is empty :)
  // also, checking if the event is based on a key pressed
  // (more specificaly the "enter" one)
  if(CAND.ui.input.value.length != 0
    && (!e.key || e.key == 'Enter')) {
    // also checking if the list already contains the new candidate
    let candidate = CAND.ui.input.value;
    if(!CAND.getCurrentCandidates().includes(candidate)) {
      if(CAND.emptyTable) {
        CAND.ui.table.classList.add('display-table')
        CAND.emptyTable = false
      }

      // adding the candidate to the table displayed bellow the form
      let cand = ['<span class="candidate">' + candidate + '</span>']
      addRowToTable(CAND.ui.table, cand)

      // also adding it as an option to the <select> in the casting section
      addCandidateToSelect(candidate)

      // resetting the input fOr a BeTtER uX
      CAND.ui.input.value = ''
    } else {
      new Popup('', [['p', 'This candidates is already in the list.']], 'error')
    }
  }
}


function validateCandidates(e) {
  /*
  API DEFINITION:
  ['cand1', 'cand2', 'cand3']
  */
  let candidates = CAND.getCurrentCandidates()

  callApi('POST', '/candidate/change', candidates, () => {successCallback(candidates)}, errorCallback)
}


/*
  The callback functions follow a similar scheme:
  Firstly, communicate information to the user (either sucess or failure)
  Secondly, update the global variable voteState
*/
function successCallback(candidates) {
  // 1. Telling the user it's OK
  new Popup('Candidates added!',
            [['p', 'The candidates you sent were taken into account.'],
             ['p', '<a class="force-link" href="#i2p">Go to the next step</a>.', 'click', goToSection]],
            'success')
  // 2. updating the global variables, saving the state and the data
  window.voteState.candidates = true
  window.voteData.candidates = candidates

}

function errorCallback(status, msg) {
  new Popup('Oh no...',
            [['p', 'The candidates you sent could not be added.'],
             ['p', 'The server responded with the following code: ' + status],
              'Message: '+ msg],
            'error')
  window.voteState.candidates = false
  window.voteData.candidates = []
}
