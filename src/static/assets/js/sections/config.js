import { Popup } from '../utils.js'
import { callApi } from '../api.js'
import { successCallback } from './casting.js'


export const CONF = {
  start() {
    this.ui = {
      'exampleButton': document.getElementById('confexample'),
      'textAreaImport': document.getElementById('confimportarea'),
      'fileInputImport': document.getElementById('confimportfile'),
      'submit': document.getElementById('confimportsubmit'),
      'buttonExportGen': document.getElementById('confexportgen'),
      'buttonExportSave': document.getElementById('confexportsave'),
      'buttonExportCopy': document.getElementById('confexportcopy'),
      'textAreaExport': document.getElementById('confexportarea'),
    }

    this.strConfig = '' // used for export

    this.setListeners()
  }, // start()

  setListeners() {
    this.ui.exampleButton.addEventListener('click', showExampleConfig)

    this.ui.submit.addEventListener('click', validateConfig)

    this.ui.buttonExportGen.addEventListener('click', generateConfig)
    this.ui.buttonExportSave.addEventListener('click', saveConfigToFile)
    this.ui.buttonExportCopy.addEventListener('click', saveCopyToClipboard)
  }
}


/*
  Showing a possible configuration to the user inside a Popup
*/
function showExampleConfig(e) {
  // the conversion from a JavaScript object to a string is a clearer approach
  // than plain HTML
  let config = {
    'candidates': ["cand1", "cand2", "cand3"],
    'locali2p': {
      'party_id': 2,
      'starting_port': 6000
    },
    'globali2p': {
      "1": "addr1.b32.i2p",
      "3": "addr3.b32.i2p",
      "4": "addr4.b32.i2p"
    },
    'casting': {
      'vote': 2,
      'date': ''
    }
  }
  config = JSON.stringify(config, null, "\t")

  new Popup('Config example:',
            [
              ['p', '<pre>' + config + '</pre>'],
              ['button', 'Copy example', 'click', ()=>{copyTextToClipboard(config)}]
          ])
}


function validateConfig(e) {
  if(CONF.ui.textAreaImport.value == '') {
    readConfigFromFile()
  } else {
    parseConfig(CONF.ui.textAreaImport.value)
  }
}


function readConfigFromFile() {
  // https://developer.mozilla.org/en-US/docs/Web/API/File/Using_files_from_web_applications
  let file = CONF.ui.fileInputImport.files[0]
  if(file) {
    let fr = new FileReader()
    fr.addEventListener('load', (e) => {
      parseConfig(e.target.result)
    })
    fr.readAsText(file)
  } else {
    new Popup('Configuration missing', [['p', 'Please copy and paste the configuration or add a file.']], 'error')
  }
}


function parseConfig(lines) {
  let config = {}
  try {
    config = JSON.parse(lines)
  } catch (e) {
    new Popup('JSON parse error',
              [['p', 'Are you sure about that JSON of yours? It seems off to me.'],
              ['p', 'Check the console for more information (F12).']],
              'error')
    console.error('Error parsing JSON:')
    console.error(e)
    return
  }

  // putting this code out of the catch so the errors can be handleded
  // differently (who does that?)
  if(checkParsedConfig(config)) {
    callAPIMadness(config)
  }
}


/*
  Only checking if all the necessary parts are present.
  It's not much, but it's honest work.
*/
function checkParsedConfig(config) {
  let res = false
  if(!config["candidates"]) {
    configError('candidates are missing.')
  } else if (!config["locali2p"]) {
    configError('your local I2P config is missing.')
  } else if(!config["globali2p"]) {
    configError('the global I2P config is missing.')
  } else if(!config["casting"]) {
    configError('the casting value is missing.')
  } else {
    res = true
  }
  return res
}

function configError(weirdPart) {
  new Popup('Config error',
            [['p', 'Your config was actually parsed, but it still looks weird. Sorry.'],
            ['p', 'That part in particular: ' + weirdPart]],
            'error')
}


/*
  Function doing all the API calls needed to simulate the ones dones via the UI
  this does the *complete* vote process
*/
function callAPIMadness(config) {
  // yolo.
  // note: for each step, saving the configuration the global voteData variable
  // so it can be retrieved later when exporting the configuration
  callApi('POST', '/candidate/change', config["candidates"], ()=>{
    window.voteState.candidates = true
    window.voteData.candidates = config["candidates"]
    callApi('POST', '/i2p/setup', config["locali2p"], ()=>{
      window.voteState.locali2p = true
      window.voteData.locali2p = config["locali2p"]
      callApi('POST', '/i2p/tunnels', config["globali2p"], ()=>{
        window.voteState.globali2p = true
        window.voteData.globali2p = config["globali2p"]
        // if everything went fine for the previous calls, let's actually vote.
        callApi('POST', '/vote/start', config["casting"], () => {
          window.voteState.casting = true
          window.voteData.casting = config["casting"]
          successCallback()
        }, errorCallback)
      }, errorCallback)
    }, errorCallback)
  }, errorCallback)
}


function errorCallback(status, msg, responseURL) {
  window.voteState.config = false
  new Popup('API error',
            [['p', 'The call for the following endpoint failed: ' + responseURL],
            ['p', 'The server responded with the following code: ' + status],
            ['p', 'Message: ' + msg]],
            'error')
}


/*
  Function generating the configuration from the window.voteData object
*/
function generateConfig(e) {
  // getting the global variable
  let data = window.voteData
  // creating an empty configuration to have something to show if incomplete
  let config = {
    'candidates': [],
    'locali2p': {
      'party_id': '',
      'starting_port': ''
    },
    'globali2p': {},
    'casting': {
      'vote': '',
      'date': ''
    }
  }

  if(data.candidates.length > 0) {
    config.candidates = data.candidates
  }

  // note: we need to test if the to be converted to object data is
  // defined (else it throws an error)
  if(data.locali2p && Object.keys(data.locali2p).length > 0) {
    config.locali2p = data.locali2p
  }

  if(data.globali2p && Object.keys(data.globali2p).length > 0) {
    config.globali2p = data.globali2p
  }

  if(data.casting && Object.keys(data.casting).length > 0) {
    config.casting = data.casting
  }

  // saving the configuration as a string for an easier use later on
  CONF.strConfig = JSON.stringify(config, null, "\t")
  CONF.ui.textAreaExport.value = CONF.strConfig
}


function emptyExportConfig() {
  new Popup('Empty configuration',
            [['p', 'You are currently trying to export an empty configuration.'],
            ['p', 'Please generate it first.']],
            'error')
}


function saveConfigToFile(e) {
  if(CONF.ui.textAreaExport.value == '' && CONF.strConfig) {
    emptyExportConfig()
  } else {
    // what the user sees is a priority, so using the text area's content first
    // and the saved-in-javascript-configuration as a fallback
    let config = CONF.ui.textAreaExport.value != '' ? CONF.ui.textAreaExport.value : CONF.strConfig

    // source: https://stackoverflow.com/questions/13405129/javascript-create-and-save-file
    // (yes)
    let file = new Blob([config], {type: 'text/json'})
    let a = document.createElement("a")
    let url = URL.createObjectURL(file)
    a.href = url
    a.download = 'yovo_config.json'
    document.body.appendChild(a)
    a.click()
    setTimeout(function() {
        document.body.removeChild(a)
        window.URL.revokeObjectURL(url)
    }, 0);
  }
}


function saveCopyToClipboard(e) {
  if(CONF.ui.textAreaExport.value == '' && CONF.strConfig) {
    emptyExportConfig()
  } else {
    // what the user sees is a priority, so using the text area's content first
    // and the saved-in-javascript-configuration as a fallback
    let config = CONF.ui.textAreaExport.value != '' ? CONF.ui.textAreaExport.value : CONF.strConfig
    copyTextToClipboard(config)
  }
}


/*
  Main copy function, using the clipboard API
*/
function copyTextToClipboard(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text)
  }

  navigator.clipboard.writeText(text).then(()=>{
    copySuccess()
  }, (err)=>{
    copyFail()
  })
}


/*
  Fallback function called if the clipboard API is not supported
  note: this should not be used in this project's context, but who knows.
*/
function fallbackCopyTextToClipboard(text) {
  let textArea = document.createElement('textarea')
  textArea.value = text
  textArea.style.position='fixed'  //avoid scrolling to bottom
  document.body.appendChild(textArea)
  textArea.focus()
  textArea.select()

  try {
    let success = document.execCommand('copy')
    if(success) {
      copySuccess()
    } else {
      copyFail()
    }
  } catch (err) {
    copyFail()
  }

  // do not forget to remove the textArea from the document's body
  document.body.removeChild(textArea)
}


function copySuccess() {
  new Popup('Configuration copied',
            [['p', 'Paste it wherever you want!']],
            'success')
}


function copyFail() {
  new Popup('Could not copy',
            [['p', 'We don\'t know what actually happened, there.']
            ['p', 'Only thing is for sure: the configuration was not copied.']],
            'success')
}
