import { Popup, addRowToTable  } from '../utils.js'
import { callApi } from '../api.js'
import { goToSection } from '../nav.js'


export const ITWOP = {
  start() {
    let section = document.getElementById('i2p')
    this.ui = {
      'portInput': document.getElementById('port'),
      'partyIdInput': document.getElementById('partyid'),
      'addrInput': document.getElementById('voteraddress'),
      'idInput': document.getElementById('voterid'),
      'addButton': document.getElementById('addvoter'),
      'submitButtonLocal': document.getElementById('sublocali2p'),
      'submitButtonVoters': document.getElementById('subvotersi2p'),
      'table': section.getElementsByTagName('table')[0],
    }
    this.emptyTable = true

    this.setListeners()
  }, // start()

  setListeners() {
    this.ui.submitButtonLocal.addEventListener('click', validateI2PLocal)
    this.ui.addButton.addEventListener('click', addVoter)
    this.ui.idInput.addEventListener('keypress', addVoter)

    this.ui.submitButtonVoters.addEventListener('click', validateI2PGlobal)
  },

  /*
    Helper returning an array of the previously entered voters' IDs
    Rather than using an internal state in JavaScript to track the IDs
    (which is hard when you want to keep track of the deletions),
    checking everytime in the DOM thanks to the custom class
  */
  getCurrentVotersIds() {
    let res = []
    for(let items of document.getElementsByClassName('voterid')) {
      res.push(items.innerHTML)
    }
    return res
  },

  getCurrentVotersAddresses() {
    let res = []
    for(let items of document.getElementsByClassName('voteraddress')) {
      res.push(items.innerHTML)
    }
    return res
  },

  /*
    Helper function checking if the party ID is present in the voters' IDs list
    Note: works with the current list. Does not check the voter's ID input, only
    the equivalent of the HTML table.
  */
  isPartyIdUsed() {
    let res = ITWOP.getCurrentVotersIds().includes(
                parseInt(ITWOP.ui.partyIdInput.value)
              )
    return res
  }
}


/*
  Helper to notice the user he has used duplicated IDs
  either in the list or his ID versus the ones in the list
*/
function uniqueIdPopup() {
  new Popup('', [['p', 'An ID has to be unique.'], ['p', 'Check your ID and the ones included in the list.']], 'error')
}


/*
  Function adding a voter (id / address) to the HTML table
*/
function addVoter(e) {
  // checking that the form is correctly populated
  // also, checking if the event is based on a key pressed
  // (more specificaly the "enter" one)
  let id = ITWOP.ui.idInput.value
  let address = ITWOP.ui.addrInput.value
  if(id.length != 0
  && address.length != 0
  && (!e.key || e.key == 'Enter')) {
    // checking that the ID is not duplicated
    // aka: already present in the voters' ID list or equals to the partyId
    if(!ITWOP.getCurrentVotersIds().includes(id) && !(ITWOP.ui.partyIdInput.value == id)) {
      if(!ITWOP.getCurrentVotersAddresses().includes(address)) {
        if(ITWOP.emptyTable) {
          ITWOP.ui.table.classList.add('display-table')
          ITWOP.emptyTable = false
        }

        // using span to later retrive the data
        let voterIdSpan = '<span class="voterid">' + id + '</span>'
        let voterAddressSpan = '<span class="voteraddress">' + address + '</span>'
        addRowToTable(ITWOP.ui.table, [voterIdSpan, voterAddressSpan])
      } else {
        new Popup('', [['p', 'You already entered this I2P address.']], 'error')
      }
    } else {
      uniqueIdPopup()
    }
  }
}


/*
  First, validating the local config against the API
*/
function validateI2PLocal(e) {
  /*
  API DEFINITION:
  {
    "party_id":2,
    "starting_port":6000,
  }
  */
  if(!ITWOP.isPartyIdUsed()) {
    let data = {
      "party_id": parseInt(ITWOP.ui.partyIdInput.value),
      "starting_port": ITWOP.ui.portInput.value,
    }
    callApi('POST', '/i2p/setup', data, () => {successConfigLocal(data)}, errorConfigLocal)
  } else {
    uniqueIdPopup()
  }
}


function successConfigLocal(localData) {
  new Popup(
    'Your configuration was saved',
    [
      ['p', 'Please restart the i2pd service and then go to your i2pd config page (by default: localhost:7070). There, go to the "I2P Tunnels" section.'],
      ['p', 'Then, get the address of the server and share it to the other voters.']
    ],
   'success')

   window.voteState.locali2p = true
   window.voteData.locali2p = localData
}


function errorConfigLocal(status, msg) {
  new Popup('Local config error',
            [
             ['p', 'The <strong>local</strong> I2P configuration you sent could not be used.'],
             ['p', 'The server responded with the following code: ' + status],
             ['p', 'Message: ' + msg]],
            'error')

  window.voteState.locali2p = false
  window.voteData.locali2p = {}
}


/*
  ... then, validate the voters' configuration against the API
*/
function validateI2PGlobal(e) {
  /*
  API DEFINITION:
  {
    "1": "addr1.b32.i2p",
    "3": "addr3.b32.i2p",
    "4": "addr4.b32.i2p",
  }
  */
  if(!ITWOP.isPartyIdUsed()) {
    let data = {}
    let ids = ITWOP.getCurrentVotersIds()
    let addresses = ITWOP.getCurrentVotersAddresses()

    for(let i = 0; i < ids.length; i++) {
      data[ids[i]] = addresses[i]
    }
    if(!(Object.keys(data).length === 0)) {
      callApi('POST', '/i2p/tunnels', data, () => {successConfigGlobal(data)}, errorConfigGlobal)
    } else {
      new Popup('', [['p', 'You are trying to send an empty list of candidates.']], 'error')
    }
  } else {
    uniqueIdPopup()
  }
}


function successConfigGlobal(globalData) {
  new Popup('The voters\' configuration was saved!',
    [
      ['p', 'Please restart the i2pd service (yes, again).'],
      ['p', '<a class="force-link" href="#casting">Go to the next step</a>.', 'click', goToSection],
    ],
   'success')

   window.voteState.globali2p = true
   window.voteData.globali2p = globalData
}


function errorConfigGlobal(status, msg) {
  new Popup('Voters\' config error',
            [['p', 'The <strong>voters\'</strong> I2P configuration you sent could not be used.'],
            ['p', 'The server responded with the following code: ' + status],
            ['p', 'Message: ' + msg]],
            'error')

  window.voteState.globali2p = false
  window.voteData.globali2p = {}
}
