import { Popup } from '../utils.js'
import { callApi } from '../api.js'
import { startResultsLoop } from './results.js'
import { goToSection } from '../nav.js'


export const CAST = {
  start() {
    this.ui = {
      'select': document.getElementById('votevalue'),
      'dateInput': document.getElementById('date'),
      'timeInput': document.getElementById('time'),
      'voteButton': document.getElementById('vote')
    }

    this.setListeners()
  }, // start()

  setListeners() {
    this.ui.voteButton.addEventListener('click', validateVote)
  }
}

/*
  This function is exported (for a cleaner organisation), and triggered each
  the user adds a candidate in the corresponding section (see ./candidates.js)
*/
export function addCandidateToSelect(candidate) {
  let option = document.createElement("option");
  option.value = candidate
  option.innerHTML = candidate

  CAST.ui.select.appendChild(option);
}

/*
  This function recreates the select from the candidates' list present in the
  relevant section
*/
export function updateCandidatesSelect() {
  // first, reset the select
  CAST.ui.select.innerHTML = '<option value="">--Please choose an option--</option>'

  // then, add the items one by one
  let table = document.getElementById('candidates-table')

  for(let cand of table.getElementsByClassName('candidate')) {
    // selecting the text value of the candidate to add it to the select
    addCandidateToSelect(cand.innerHTML)
  }
}

function validateVote(e) {
  /*
  API DEFINITIONS:
  {
    "vote": 3,
    "date": "YYYY-MM-DD HH:MM"
  }

  note: "date" can be empty for an instant start
  */
  if(window.voteState.isReady()) {
    let d = CAST.ui.dateInput.value
    let t = CAST.ui.timeInput.value
    let data = {
      "vote": CAST.ui.select.selectedIndex -1, // there's the default option!
      "date": d + " " + t
    }
    callApi('POST', '/vote/start', data, () => {successCallback(data)}, errorCallback)
  } else {
    if(!window.voteState.candidates) {
      new Popup('Candidates configuration missing',
                [['p', 'Please configure the candidates before trying to vote.']],
                'error')
    } else if(!window.voteState.locali2p) {
    new Popup('Local I2P configuration missing',
              [['p', 'Please setup the <strong>local</strong> I2P configuration before trying to vote.']],
              'error')
    } else if(!window.voteState.globali2p) {
      new Popup('Voters\'s I2P configuration missing',
                [['p', 'Please setup the <strong>voters\'</strong> I2P configuration before trying to vote.']],
                'error')
    }
  }
}


/*
  This function is exported as it's used in config.js
*/
export function successCallback(castingData) {
  new Popup('You just voted!',
            [
              ['p', 'Now you have to wait for MP-SPDZ to do its magic.'],
              ['p', '<a class="force-link" href="#results">Go to the results\' page</a>.', 'click', goToSection]
            ],
            'success')

  startResultsLoop()

  window.voteState.casting = true
  window.voteData.casting = castingData
}


function errorCallback(status, msg) {
  new Popup('Oh no...',
            [['p', 'The vote could not start.'],
            ['p', 'The server responded with the following code: ' + status],
            ['p', 'Message: ' + msg]],
            'error')

  window.voteState.casting = false
  window.voteData.casting = {}
}
