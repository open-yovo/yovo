import { callApi } from '../api.js'
import { Popup, addRowToTable } from '../utils.js'


export const RES = {
  start() {
    let section = document.getElementById('results')

    this.ui = {
      'info': document.getElementById('results-info'),
      'stopElements': document.getElementsByClassName('stoprequests'),
      'stopButton': document.getElementById('stoprequests'),
      'table': section.getElementsByTagName('table')[0],
    }
    // counting the number of times the votes was retrieved
    this.times = 0
    // interval for requests retrieving the vote's result
    this.interval = 2000
    // boolean used to stop the requests to retrieve the vote's results
    this.stop = false

    this.setListeners()
  }, // start()

  setListeners() {
    this.ui.stopButton.addEventListener('click', stopRequests)
  },
}


/*
  Main function used to start the 'get the results pls' loop
*/
export function startResultsLoop() {
  // setting the boolean to its initial state if the vote has already been started before
  RES.stop = false

  // starting the recursive calls and counting them
  waitForResults()

  // showing the button to stop the results calls
  for(let e of RES.ui.stopElements) {
    e.style.display = 'initial'
  }
}


/*
  Stopping the results loop
*/
function stopRequests(e) {
  RES.stop = true

  callApi('GET', '/vote/stop', '', stopSuccess, stopError)

  // using the same interval, to be sure the message is updated after the last
  // API call, as it updates it with "waiting for results..." :)
  setTimeout(() => {
    updateWaitingMessage('The results will appear here; please cast a vote and come back.')

    // hidding the buttons and stuff about stopping the calls
    // while the votes has not started again
    for(let e of RES.ui.stopElements) {
      e.style.display = 'none'
    }
  }, RES.interval)
}


function stopSuccess() {
  new Popup('No more requests',
            [['p', 'Results won\'t be retrieved from the file system and the vote process has been stopped.'],
            ['p', 'You can restart the web server and the vote process.']])
}


function stopError(status, msg) {
  new Popup('Could not really stop',
            [
              ['p', 'The request to retrieve the results from the file system have stopped, but we could not stop the process itself.'],
              ['p', 'The server responded with the following code: ' + status],
              ['p', 'Message: ' + msg]
            ],
            'error')
}


function updateWaitingMessage(msg) {
  RES.ui.info.innerHTML = msg
}


function waitForResults() {
  RES.times++
  updateWaitingMessage('Waiting for results... Check number ' + RES.times + '. Checking every ' + RES.interval + 'ms.')
  callApi('GET', '/vote/results', '', waitSucess, waitError)
}


function waitError(status, msg) {
  new Popup('The results could not be retrieved',
            [['p', 'An unfortunate, last minute error occured, sorry.'],
            ['p', 'The server responded with the following code: ' + status],
            ['p', 'Message: ' + msg]],
            'error')
}


function waitSucess(data) {
  let candidates = JSON.parse(data)
  if (Object.keys(candidates).length == 0) {
    // while the returned objects is empty, and while the user didn't specify
    // anything (aka: s/he didn't ask the program to stop the resquests),
    // we're making a recursive call every 2 seconds
    if(RES.stop == false) {
      setTimeout(() => {
        waitForResults()
      }, RES.interval)
    }
  } else {
    // removing the stop requests contents
    for(let e of RES.ui.stopElements) {
      e.style.display = 'none'
    }
    // resetting the number of checks
    RES.times = 0

    // clear the table if previous results were already stored here
    RES.ui.table.getElementsByTagName('tbody')[0].innerHTML = ''

    // showing the table
    RES.ui.table.classList.add('display-table')

    for(let key of Object.keys(candidates)) {
      // adding the candidate and its results to the table,
      // but w/out a cross :)
      addRowToTable(RES.ui.table, [key, candidates[key]], false)
    }
  }
}
