/*
  Helper function adding a row to a specified table.
  Creates a cell for each elements provided.
  If cross == true, then another cell with a cross is added
  This cross is then used to delete the row on click
*/
export function addRowToTable(table, elements, cross = true) {
  // prepare each element
  let tr = document.createElement('tr')
  for(let elem of elements) {
    let td = document.createElement('td')
    td.innerHTML = elem
    tr.appendChild(td)
  }

  // if there's a cross, then add it
  if(cross) {
    let cross = document.createElement('td')
    cross.innerHTML = 'x'
    cross.classList.add('cross')
    tr.appendChild(cross)
    cross.addEventListener('click', removeRowFromTable)
  }

  // actually append the new table's row to its body
  let tbody = table.getElementsByTagName('tbody')[0]
  tbody.appendChild(tr)
}


function removeRowFromTable(e) {
  let tr = e.target.parentNode
  let tbody = tr.parentNode

  tbody.removeChild(tr)
}


/*
  The class used to create the beautiful popups displaying some more or less
  valuable informations to the user :)
*/
export class Popup {
  /*
    title*: the title of the popup (displayed at the top as an h2)
    elements*: an array of elments such as [type, content, eventListener, callback]
                eg: ['button', 'click me', 'click', (e)=>{alert('button clicked!')}]
    status*: the status (error / success) of the popup

    *: otionnal
  */
  constructor(title, elements, status) {
    this.title = title
    this.elements = elements
    this.status = status
    this.wrapper = document.getElementsByClassName('popup-wrapper')[0]
    this.content = this.wrapper.getElementsByClassName('popup')[0]

    // this needs to be bound as we want to access to the class' properties
    this.closeFromClick = this.closeFromClick.bind(this)
    this.closeFromKeydown = this.closeFromKeydown.bind(this)

    // closing the popup on click outsite of it (the wrapper covers the page)
    this.wrapper.addEventListener('click', this.closeFromClick)
    // closing if the escape key is pressed
    document.addEventListener('keydown', this.closeFromKeydown)
    // by default, opening the popup
    this.open()
  }

  open() {
    // resetting the content in case the popup has been already used
    this.content.innerHTML = '<span class="popup-cross">x</span>'

    if(this.title) {
      let h2 = document.createElement('h2')
      h2.innerHTML = this.title
      this.content.appendChild(h2)
    }

    if(this.elements.length > 0) {
      for(let elem of this.elements) {

        // for each elements provided, we create a new HTML element of the
        // specified type
        let e = document.createElement(elem[0])
        // then we populate its content
        e.innerHTML = elem[1]
        this.content.appendChild(e)

        // if an event listener has been provided with a callback, we add it
        if(elem[2] && elem[3]) {
          e.addEventListener(elem[2], elem[3])
        }
      }
    }

    if(this.status) {
      let span = document.createElement('span')
      span.classList.add('popup-status')
      // the class name is synchronised with the status (cf core.css)
      span.classList.add(this.status)
      if(this.status == 'success') {
        span.innerHTML = '✔'
      } else if(this.status == 'error') {
        span.innerHTML = '!'
      } else {
        span.innerHTML = '?'
      }

      this.content.appendChild(span)
    }

    this.wrapper.classList.add('display-popup')
  }

  closeFromClick(e) {
    // closing only if the click is done on the cross
    // or if done outside the popup, aka on the wrapper (as it's full screen)
    if(e.target == this.wrapper
      || e.target.classList.contains('popup-cross')) {
        this.close()
    }
  }

  closeFromKeydown(e) {
    if(e.key == 'Escape') {
      this.close(e)
    }
  }

  close() {
    // removing the event listeners for performance (yeah right)
    this.wrapper.removeEventListener('click', this.closeFromClick)
    document.removeEventListener('keydown', this.closeFromKeydown)

    this.wrapper.classList.remove('display-popup')
  }
}
