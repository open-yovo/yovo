import {updateCandidatesSelect} from './sections/casting.js'

export const NAV = {
  start() {
    this.ui = {
      'sections': {
        'candidates': document.getElementById('candidates'),
        'i2p': document.getElementById('i2p'),
        'casting': document.getElementById('casting'),
        'results': document.getElementById('results'),
        'config': document.getElementById('config')
      },
      'navMenuItems': document.querySelectorAll('.nav-menu li a')
    }
    this.currentSection = Object.values(this.ui.sections)[0]
    this.setListeners()
  }, // start()

  setListeners() {
    for(let link of document.querySelectorAll('.nav-menu li a')) {
      link.addEventListener('click', goToSection)
    }
  },
}

/*
  Getting the relevant hash to go to the section by its id
*/
export function goToSection(e) {
  e.preventDefault()
  // closing a potential popup before going to the next section
  // to do so, we trigger a click event outside the wrapper ¯\_(ツ)_/¯
  document.getElementsByClassName('popup-wrapper')[0].dispatchEvent(new Event('click'))

  let hash = e.target.getAttribute('href').split('#').pop()
  swapSections(hash, NAV.currentSection, NAV.ui.sections[hash])
}

/*
  Helper function used to swap between 2 sections, while updating the logic
*/
function swapSections(hash, previousSection, nextSection) {
  previousSection.classList.remove('current-section')
  nextSection.classList.add('current-section')

  NAV.currentSection = nextSection

  // adding a small visual cue for the current sections
  for(let item of NAV.ui.navMenuItems) {
    if(item.getAttribute('href') == '#' + hash) {
      item.classList.add('current-item')
    } else {
      item.classList.remove('current-item')
    }
  }

  if(NAV.currentSection == NAV.ui.sections.casting) {
    /*
      We gotta update the candidates select in the "casting" section based on
      their list displayed in the "Candidates" section
      There was a choice to be made early on: either do it everytime the list
      is updated, or everytime the relevant section is displayed.
      For an easier development, it is made everytime the section is displayed.
    */
    updateCandidatesSelect()
  }
}
