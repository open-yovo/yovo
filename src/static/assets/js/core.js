import { NAV } from './nav.js'

import { CONF } from './sections/config.js'
import { CAND } from './sections/candidates.js'
import { ITWOP } from './sections/i2p.js'
import { CAST } from './sections/casting.js'
import { RES } from './sections/results.js'


/*
  Global variable used to track the current vote's state and datas

  note: needs to be defined as a window property for an easier use in imported
  files. :)

  see frontend.md as to why we're not using the localStorage API
*/
window.voteState = {
  'candidates': false,
  'locali2p': false,
  'globali2p':false,
  'isReady': () => {
    return (window.voteState.candidates && window.voteState.locali2p && window.voteState.globali2p)
  }
}

window.voteData = {
  candidates: [],
  locali2p: {},
  globali2p: {},
  casting: {}
}

/*
  Starting point of the app
*/
window.addEventListener('load', function(e) {
  console.log("YOVO starts here.")

  NAV.start()

  CONF.start()
  CAND.start()
  ITWOP.start()
  CAST.start()
  RES.start()
})
