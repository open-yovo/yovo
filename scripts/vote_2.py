class Scrutin:

	def __init__(self, nb_votants, nb_candidats):
		self.nb_votants = nb_votants
		self.digit_canditat = len(str(nb_votants)) + 1
		self.nb_candidats = nb_candidats
		self.ballot = 0

	def vote(self, candidat):
		if 0 < candidat <= self.nb_candidats:
			self.ballot += int("1" + "0" * (candidat - 1) * self.digit_canditat)
			print("Vote pour :", candidat)
		else:
			print("candidat invalide")

	def show_result(self):
		print("Ballot final :", self.ballot)
		
		tmp_ballot = self.ballot
		padding = int("1" + "0" * self.digit_canditat)

		for i in range(self.nb_candidats):
			cpt = 0
			while tmp_ballot % padding != 0:
				cpt += 1
				tmp_ballot -= 1
			print(cpt, "pour le candidat", i + 1)
			tmp_ballot = tmp_ballot / padding

if __name__ == '__main__':
	scrutin = Scrutin(82, 3)
	scrutin.vote(1)
	scrutin.vote(2)
	scrutin.vote(1)
	scrutin.vote(3)
	scrutin.vote(2)
	scrutin.vote(2)
	scrutin.vote(3)
	scrutin.show_result()