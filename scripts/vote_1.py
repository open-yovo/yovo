choix = [3, 5, 7, 11, 13]
resultat = 3**2 * 5**1 * 7**0 * 11**5 * 13

print("Total initial : ", resultat)
tmp = resultat
votes = {}
for candidat in choix:
	votes[candidat] = 0
	while tmp % candidat == 0:
		tmp = tmp / candidat
		votes[candidat] += 1

if tmp != 1:
	print("Erreur")
else:
	for candidat in votes:
		print(votes[candidat], "pour le candidat", candidat)