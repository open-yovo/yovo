# -*- coding: utf-8 -*-
""" Generate a badge to count vulnerabilities. """

import anybadge
import datetime
import json
import os
import pytz
import subprocess
import sys

THRESHOLDS_1 = {
    3: 'red',
    5: 'orange',
    7: 'yellow',
    10: 'green'
}

THRESHOLDS_2 = {
    1: 'green',
    4: 'yellow',
    6: 'orange',
    10: 'red'
}


def execute_command(command):
    """
	Execute a command in host shell
	Parameters:
		command (list): Words composing the command
	Returns:
		stdout (str): Standard command output
		stderr (str): Error command output
	"""

    result = subprocess.Popen(
    	command[0],
    	stdout=subprocess.PIPE,
    	stderr=subprocess.PIPE,
    	shell=True
    )
    stdout, err = result.communicate()
    result.wait()
    sys.stdout.flush()
    return (stdout.decode('UTF-8'), err.decode('UTF-8'))

def parse_pylint_score(output):
    """
    Parse pylint score from analysis result.

	Returns:
		score (str): Score parsed from file
    """

    end = output.find("/ 10")
    start = output.find(":strong:`")
    score = output[start+9:end-1]
    return score

def pylint_exec():
    """
    Execute pylint and generate badge.
	
	Returns:
		score (str): Score of the analysis
    """

    os.chdir('src')
    print("Analysing code with pylint ...\n")
    command = "pylint yovo > pylint.json"
    pylint_out, err = execute_command([command])
    if err:
        print(err)

    out, err = execute_command(['pylint-json2html -f jsonextended -t {} -o {} ./pylint.json'.format(
    	'pylint_report.rst',
    	'../docs/annexes/pylint.rst'
    )])

    if err:
        print(err)
    os.chdir('..')
    rst = open('docs/annexes/pylint.rst', 'r').read()
    score = parse_pylint_score(rst)
    print("Your code has been rated at {}/10".format(score))

    badge = anybadge.Badge('code review', float(score), thresholds=THRESHOLDS_1)
    badge.write_badge('pylint.svg')

    return score

def main():
    """ Main function """

    score = pylint_exec()
    vulns_nb = len(execute_command(["safety check --bare"])[0].split(' ')) -1

    badge2 = anybadge.Badge(
        'dependency vulns',
        vulns_nb,
        thresholds=THRESHOLDS_2,
        value_format="%d"
    )
    badge2.write_badge('vulns.svg')

    print("Number of vulnerable depencies :", vulns_nb)
    vulns = execute_command(["safety check"])[0]
    vulns_txt = open("vulns.txt", "w")
    vulns_txt.write(vulns)
    vulns_txt.close()

    exit_code = 0
    if float(score) < 7 or vulns_nb > 3:
        print("Analyse score is too low (<7) or number of vulnerable depencies is too high (>3)")

    return exit_code

if __name__ == "__main__":
    sys.exit(main())
