import datetime
import os
import pytz

from discord_webhook import DiscordWebhook, DiscordEmbed

WEBHOOK = "https://discordapp.com/api/webhooks/626148734639669266/ZDse6Ejg7IceQqSmQR0xuY3G9tFBtktF6FgIOT2VXm0edRjy032pyEhoOjpe6j2erQ-y"

def notify():
    """ Execute discord webhhok with commit info. """

    time_naive = datetime.datetime.now()
    timezone = pytz.timezone('Europe/Paris')
    time_aware = timezone.localize(time_naive)

    embed = DiscordEmbed(
        title=os.environ.get('CI_COMMIT_SHA'),
        description=os.environ.get('CI_COMMIT_MESSAGE'),
        color=242424
    )
    embed.set_url("https://gitlab.com/open-yovo/yovo/tree/{}".format(os.environ.get('CI_COMMIT_SHA')))
    embed.set_footer(text="From GitLab Runner")
    embed.set_timestamp()
    embed.add_embed_field(name="📅 Date", value=time_aware.strftime('%d/%m/%Y'))
    embed.add_embed_field(name="🕐 Heure", value=time_aware.strftime('%H:%M'))
    embed.set_author(
        name="{} <{}>".format(
            os.environ.get("GITLAB_USER_NAME"),
            os.environ.get("GITLAB_USER_EMAIL"),
        ),
        url="https://gitlab.com/{}".format(os.environ.get('GITLAB_USER_NAME')),
    )

    webhook = DiscordWebhook(url=WEBHOOK)
    webhook.add_embed(embed)
    webhook.execute()

if __name__ == '__main__':
	notify()